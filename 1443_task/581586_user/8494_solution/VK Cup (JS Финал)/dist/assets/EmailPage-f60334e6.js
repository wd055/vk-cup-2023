import{s as u,c as v,u as m,j as e,n as r,a as i,f as b,t as y,g as p,i as g,p as w,b as o,h as k,m as S}from"./index-fde7ef1c.js";import{S as z,d as D,L as f,o as E,f as j,r as $,t as I,a as P,b as F,e as L,h as C}from"./apiRequests-d13b4fe5.js";const N=u(z)`
  display: flex;
  flex-direction: column;
  padding-left: 32px;
  padding-right: 32px;
  position: relative;

  & > div:first-child {
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 60px;

    h1 {
      font-size: 20px;
      font-weight: bold;
      margin-top: 16px;
      margin-bottom: 16px;
    }
    div {
      font-size: 13px;
      display: flex;
      align-items: center;
      & > p {
        margin-left: 8px;
      }
    }
  }

  & > div:nth-child(2) {
    margin-bottom: 12px;
    height: 60px;
    position: relative;
    display: flex;
    align-items: center;

    & > div:nth-child(3) {
      & > div:nth-child(1) {
        & > span:nth-child(2) {
          color: ${({theme:t})=>t.text.secondary};
        }
      }
    }

    div:nth-child(1) {
      display: flex;
      align-items: center;
      position: relative;
      span {
        margin-right: 10px;
      }
      & > img {
        position: absolute;
        left: -28px;
      }
    }
    img {
      margin-right: 8px;
    }
  }

  & > div:nth-child(2) div:nth-child(3) {
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    div:nth-child(1) {
      span:first-child {
        font-size: 15px;
      }
      span:nth-child(2) {
        font-size: 13px;
      }
    }

    div:last-child {
      font-size: 13px;
      color: ${({theme:t})=>t.text.secondary};
      margin-top: 5px;
    }
  }

  & > div:nth-child(2) div:nth-child(2) {
    img {
      width: 32px;
      height: 32px;
      border-radius: 100%;
    }
  }

  & > div:last-child {
    padding-top: 16px;
    padding-bottom: 16px;

    p {
      line-height: 1.5rem;
      font-size: 15px;
    }
  }

  & > div:nth-child(3) {
    img {
      width: 256px;
      height: 190px;
      border-radius: 12px;
    }

    & > div:first-child {
      display: flex;
      justify-content: flex-start;
      & > img:first-child {
        margin-right: 10px;
      }
    }

    & > div {
      font-size: 13px;
      margin-top: 8px;
      margin-bottom: 8px;
      span {
        color: ${({theme:t})=>t.text.secondary};
      }
      a {
        color: #005bd1;
      }
      a:hover {
        opacity: 0.8;
      }
    }
  }
  .imgLoader {
    position: relative;
    width: 100%;
    height: 150px;
    span {
      justify-content: start;
      margin-left: 150px;
    }
    svg {
      width: 50px;
      height: 50px;
    }
  }
`;function A(t){const{t:n}=v(),d=m(a=>a.utils.lang),l={Заказы:E,Финансы:j,Регистрации:$,Путешествия:I,Билеты:P,"Штрафы и налоги":F};return e(r,{children:[e("div",{children:[i("h1",{children:t.title}),i("div",{children:t.flag||""in l?e(r,{children:[i("img",{src:l[t.flag||""],alt:"flag"}),i("p",{children:n(`flags.${t.flag}`)})]}):!1})]}),e("div",{children:[i("div",{children:t.read===!1?i("img",{src:b,alt:"status"}):""}),i("div",{children:i("img",{src:t.author.avatar||D,alt:"avatar"})}),e("div",{children:[e("div",{children:[e("span",{children:[t.author.name," "]}),e("span",{children:[Number(new Date)-Number(new Date(t.date))<864e5?`${n("other.today")},`:""," ",y(new Date(t.date),d)]}),i("span",{children:t.bookmark&&t.important?e(r,{children:[i("img",{src:p,alt:"status"})," ",i("img",{src:g,alt:"status"})]}):t.bookmark?i("img",{src:p,alt:"status"}):t.important?i("img",{src:g,alt:"status"}):!1})]}),e("div",{children:[i("span",{children:n("other.sentTo")+": "}),t.to.map((a,h,c)=>e("span",{children:[a.name,c.slice(-1)[0].email!==a.email?", ":""]},a.email))]})]})]}),i("div",{children:t.doc&&t.doc[0].img?e(r,{children:[i("div",{children:t.doc.map(a=>i("img",{src:a.img,alt:"doc"},a.img.slice(0,10)))}),e("div",{children:[t.doc.length," ",n("other.filesQuantity")," ",t.doc.length===1?e("a",{href:t.doc[0].img,download:"doc.png",children:[n("other.downloadFile")," "]}):e("a",{href:t.doc[0].img,download:"doc.png",children:[n("other.downloadFiles")," "]}),i("span",{children:w(t.doc)})]})]}):t.img&&t.img[0]===!0?i("div",{className:"imgLoader",children:i(f,{})}):""}),i("div",{children:i("p",{children:t.text})})]})}function O(){const[t,n]=o.exports.useState(null),[d,l]=k(),[a,h]=o.exports.useState(void 0),c=m(s=>s.utils.isImages);return o.exports.useEffect(()=>{const s=`/email?title=${d.get("title")}`;S({fetchData:C,urlString:s,functions:[n]}),document.title="WebMail - Письмо"},[]),o.exports.useEffect(()=>{c!=null&&(async()=>{const s=`/email?title=${d.get("title")}&imgs=true`,x=await L(s);h(x)})()},[]),i(r,{children:i(N,{children:t?i(A,{author:t.author,folder:t.folder,bookmark:t.bookmark,date:t.date,to:t.to,title:t.title,important:t.important,text:t.text,read:t.read,flag:t.flag,doc:a,img:t.doc},t.date):i(f,{})})})}export{O as default};
