import{b as l,s as B,F as P,u as I,c as O,j as p,a as e,d as _,L as j,e as A,f as T,g as R,i as N,S as W,t as G,h as q,k as H,l as J,m as K,n as Q}from"./index-fde7ef1c.js";import{d as U,o as X,f as Y,r as Z,t as tt,a as et,b as nt,g as M,S as rt,L as it,c as at}from"./apiRequests-d13b4fe5.js";var L=new Map,$=new WeakMap,F=0,st=void 0;function ot(t){return t?($.has(t)||(F+=1,$.set(t,F.toString())),$.get(t)):"0"}function dt(t){return Object.keys(t).sort().filter(function(i){return t[i]!==void 0}).map(function(i){return i+"_"+(i==="root"?ot(t.root):t[i])}).toString()}function ct(t){var i=dt(t),n=L.get(i);if(!n){var s=new Map,a,u=new IntersectionObserver(function(f){f.forEach(function(o){var c,d=o.isIntersecting&&a.some(function(h){return o.intersectionRatio>=h});t.trackVisibility&&typeof o.isVisible>"u"&&(o.isVisible=d),(c=s.get(o.target))==null||c.forEach(function(h){h(d,o)})})},t);a=u.thresholds||(Array.isArray(t.threshold)?t.threshold:[t.threshold||0]),n={id:i,observer:u,elements:s},L.set(i,n)}return n}function lt(t,i,n,s){if(n===void 0&&(n={}),s===void 0&&(s=st),typeof window.IntersectionObserver>"u"&&s!==void 0){var a=t.getBoundingClientRect();return i(s,{isIntersecting:s,target:t,intersectionRatio:typeof n.threshold=="number"?n.threshold:0,time:0,boundingClientRect:a,intersectionRect:a,rootBounds:a}),function(){}}var u=ct(n),f=u.id,o=u.observer,c=u.elements,d=c.get(t)||[];return c.has(t)||c.set(t,d),d.push(i),o.observe(t),function(){d.splice(d.indexOf(i),1),d.length===0&&(c.delete(t),o.unobserve(t)),c.size===0&&(o.disconnect(),L.delete(f))}}function ut(t){var i,n=t===void 0?{}:t,s=n.threshold,a=n.delay,u=n.trackVisibility,f=n.rootMargin,o=n.root,c=n.triggerOnce,d=n.skip,h=n.initialInView,w=n.fallbackInView,v=n.onChange,b=l.exports.useState(null),x=b[0],k=b[1],S=l.exports.useRef(),E=l.exports.useState({inView:!!h,entry:void 0}),r=E[0],g=E[1];S.current=v,l.exports.useEffect(function(){if(!(d||!x)){var y;return y=lt(x,function(C,D){g({inView:C,entry:D}),S.current&&S.current(C,D),D.isIntersecting&&c&&y&&(y(),y=void 0)},{root:o,rootMargin:f,threshold:s,trackVisibility:u,delay:a},w),function(){y&&y()}}},[Array.isArray(s)?s.toString():s,x,o,f,c,d,u,w,a]);var V=(i=r.entry)==null?void 0:i.target;l.exports.useEffect(function(){!x&&V&&!c&&!d&&g({inView:!!h,entry:void 0})},[x,V,c,d,h]);var m=[k,r.inView,r.entry];return m.ref=m[0],m.inView=m[1],m.entry=m[2],m}const ht=B(P)`
  flex-direction: column;
  justify-content: center;
  width: 100%;
  height: 100%;
  h1 {
    margin-top: 20px;
    font-size: 24px;
    font-weight: bold;
  }
`,pt="/assets/noLetters-f7c894c1.png",ft="/assets/noLettersDark-7b43bc8f.png",gt="/assets/notFound-21bab61e.svg";function vt(){const t=I(n=>n.utils.theme),{t:i}=O();return p(ht,{children:[e("img",{src:t==="dark"?ft:t==="anime"?gt:pt,alt:"no letters"}),e("h1",{children:i("other.noLetters")})]})}const xt="/assets/checked-2dd501e3.svg",mt=B.div`
  background-color: ${({theme:t})=>t.container};
  position: relative;
  height: 48px;
  padding-left: 12px;
  padding-right: 12px;
  font-size: 15px;
  transition: 0.4s;

  div {
    min-width: 0;
  }

  div:first-child {
    .greyDot {
      display: none;
    }
  }

  div:nth-child(2) {
    img {
      width: 32px;
      height: 32px;
      border-radius: 100%;
      scale: 1;
      transition: 0.4s;
    }
    input {
      width: 16px;
      height: 16px;
      position: absolute;
      left: 36.5px;
      bottom: 15px;
      scale: 0;
      transition: 0.4s;
      appearance: none;
      cursor: pointer;
    }
    input:checked {
      scale: 1;
    }
    input:checked::after {
      content: url(${xt});
      position: absolute;
      bottom: 100px;
      border: none;
    }
    input:checked::before {
      content: "";
      position: absolute;
      background-color: ${({theme:t})=>t.container};
      inset: 0;
      margin: -10px;
      z-index: 999;
      transition: 0.4s;
    }
    input::after {
      content: "";
      position: absolute;
      inset: 0;
      border: 1px solid ${({theme:t})=>t.checkbox||"#e0e2e8"};
      border-radius: 4px;
      z-index: 9999;
    }
  }

  div:nth-child(3) {
    margin-left: 5px;
  }
  div:nth-child(4) {
    display: flex;
    justify-content: center;

    .addBookmark {
      display: none;
    }
    span {
      display: flex;
    }
  }
  div:nth-child(5) {
    margin-left: 5px;
  }

  div:nth-child(3),
  div:nth-child(5),
  div:nth-child(6),
  div:nth-child(8) {
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  div:nth-child(6) {
    color: ${({theme:t})=>t.text.secondary};
  }

  div:nth-child(7) {
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    right: 0;
    padding-right: 100px;
    background-color: ${({theme:t})=>t.container};
    z-index: 999;
    transition: 0.4s;
    span:first-child {
      padding-left: 16px;
    }
    span:last-child {
      padding-left: 10px;
    }
  }

  div:nth-child(8) {
    position: absolute;
    background-color: ${({theme:t})=>t.container};
    width: max-content !important;
    right: 0;
    padding-right: 12px;
    padding-left: 16px;
    z-index: 999;
    transition: 0.4s;
    span {
      color: ${({theme:t})=>t.text.secondary};
      font-size: 13px;
    }
  }

  .unread {
    font-weight: bold;
  }

  display: grid;
  grid-template-columns: 20px 36px 200px 40px max-content 1fr 50px 70px;
  grid-template-rows: 1fr;
  grid-column-gap: 1px;
  grid-row-gap: 0px;
  align-items: center;

  @media (max-width: 900px) {
    grid-template-columns: 20px 36px 200px 40px max-content 1fr 50px 70px;
  }

  &:hover {
    background-color: ${({theme:t})=>t.hoveredItem};
    transition: 0.4s;
    border-radius: 12px;
    cursor: pointer;

    div:first-child {
      .greyDot {
        display: block;
      }
    }
    div:nth-child(2) {
      img {
        scale: 0;
        transition: 0.4s;
      }
      input {
        scale: 1;
        transition: 0.4s;
      }
      input:checked::before {
        background-color: ${({theme:t})=>t.hoveredItem};
        transition: 0.4s;
      }
    }
    div:nth-child(4) {
      .addBookmark {
        display: inline;
      }
    }
    div:nth-child(7),
    div:nth-child(8) {
      background-color: ${({theme:t})=>t.hoveredItem};
      transition: 0.4s;
    }
  }

  hr {
    position: absolute;
    height: 1px;
    border: none;
    width: 95%;
    right: 5px;
    background-color: ${({theme:t})=>t.hr};
    bottom: 0;
    margin: auto;
  }
`,z="/assets/addBookmark-706ee389.svg",bt="/assets/grayDot-9c2aa916.svg";function kt(t){const i=I(a=>a.utils.lang),n=_(),s={Заказы:X,Финансы:Y,Регистрации:Z,Путешествия:tt,Билеты:et,"Штрафы и налоги":nt};return e(j,{to:"/email?title="+t.title,children:p(mt,{onClick:()=>n(A(t.doc)),children:[p("div",{onClick:a=>a.preventDefault(),children:[t.read===!1?e("img",{src:T,alt:"status"}):e("img",{src:bt,alt:"status",className:"greyDot"}),e("div",{})]}),p("div",{children:[e("img",{src:t.author.avatar||U,alt:"avatar"}),e("input",{onClick:a=>a.stopPropagation(),type:"checkbox"})]}),e("div",{children:p("p",{className:t.read===!1?"unread":"",children:[t.author.name," ",t.author.surname]})}),e("div",{onClick:a=>a.preventDefault(),children:t.bookmark&&t.important?p("span",{children:[e("img",{src:R,alt:"status"})," ",e("img",{src:N,alt:"status"})]}):t.bookmark?e("img",{src:R,alt:"status"}):t.important?p("span",{children:[e("img",{src:z,alt:"add_bookmark",className:"addBookmark"}),e("img",{src:N,alt:"status"})]}):e("img",{src:z,alt:"add_bookmark",className:"addBookmark"})}),p("div",{className:t.read===!1?"unread":"",children:[t.title,"  "]}),e("div",{children:t.text}),p("div",{children:[t.flag in s?e("span",{children:e("img",{src:s[t.flag],alt:"flag"})}):!1,e("span",{children:t.doc?e(W,{}):!1})]}),e("div",{children:e("span",{children:G(new Date(t.date),i)})}),e("hr",{})]})})}function St(){const[t,i]=l.exports.useState(null),[n,s]=l.exports.useState(20),[a,u]=l.exports.useState(null),[f,o]=l.exports.useState(2),c=I(r=>r.utils.isLoading),d=I(r=>r.utils.theme),[h,w]=q(),{folder:v}=H(),{t:b}=O(),{ref:x,inView:k,entry:S}=ut(),E=l.exports.useRef(null);return l.exports.useEffect(()=>{k&&n===20&&(async()=>{o(f+1);const r=await M(`/${v}/${f}`);s(r.length),i(g=>[...g,...r]),u(g=>[...g,...r])})()},[k,S]),l.exports.useEffect(()=>{J({emails:a,searchParams:h,setEmails:i,setParams:w})},[h,a]),l.exports.useEffect(()=>{v&&(document.title=`WebMail - ${b(`navbar.${v}`)}`)},[v,b]),l.exports.useEffect(()=>{var g;o(2),s(20),(g=E.current)==null||g.scrollTo({top:0,behavior:"auto"});const r=`/${v}/1`;K({fetchData:M,urlString:r,functions:[i,u]}),w("?letters=all")},[v]),e(rt,{className:d==="anime"?"animeNotFound":"",ref:E,children:c&&k===!1?e(it,{svgFill:d==="anime"?"#ffffff":void 0}):(t==null?void 0:t.length)===0?e(vt,{}):p(Q,{children:[t==null?void 0:t.map(r=>e(kt,{author:r.author,folder:r.folder,bookmark:r.bookmark,date:r.date,to:r.to,title:r.title,important:r.important,text:r.text,read:r.read,flag:r.flag,doc:r.doc},r.date)),e("div",{ref:x,className:"moreEmailsLoader",children:k&&n===20?e(at,{}):n<20?e("p",{children:b("other.noMoreLetters")}):!1})]})})}export{St as default};
