import {useState, useEffect, useRef, useContext} from "preact/hooks"
import {SERVER_URL} from "../index"
import {GlobalKeyValueEventBus} from "../context"

import { FormatMessage } from "../../Intl"

import { FOLDERS } from "../constraints"


const LetterContextMenu = ({letterData, folder, state, setLetter, pos}) => {
	const [moveToModalOpen, setMoveToModalOpen] = useState(false);
	const globalKeyValueEventBus = useContext(GlobalKeyValueEventBus);
	const isFetching = useRef(false)
	const contextMenuRef = useRef();
	const foldersDropdownRef = useRef();


	useEffect(() => {
		if(moveToModalOpen){
			const rms = window.innerWidth - (contextMenuRef.current.offsetWidth + pos.x);
			if(rms > 0){
				foldersDropdownRef.current.classList.add("letter__context__menu__move__to__dropdown__left")
			}
			else{
				foldersDropdownRef.current.classList.add("letter__context__menu__move__to__dropdown__right")
			}
			
		}
	}, [moveToModalOpen])

	useEffect(() => {

		const KeyDownEvent = (e) => {
			const t = e.target;
			if(!contextMenuRef.current.contains(t)){
				globalKeyValueEventBus.emit("contextMenuClose");
				state(false)
			}
		}

		const cmw =  contextMenuRef.current.offsetWidth;
		const cmh = contextMenuRef.current.offsetHeight;
		
		const x = pos.x + cmw > window.innerWidth ? (window.innerWidth - cmw - 50) : (pos.x < cmw + 50 ? cmw + 50 : pos.x);
		const y = pos.y + cmh  > window.innerHeight ? (pos.y - cmh) : pos.y

		contextMenuRef.current.style.left = x + "px"
		contextMenuRef.current.style.top = y + "px"

		window.addEventListener("mousedown", KeyDownEvent)
		return () => {
			globalKeyValueEventBus.emit("contextMenuClose");
			window.removeEventListener("mousedown", KeyDownEvent)
		};
	}, [])


	const MoveToFolder = (slug) => {
		if(!isFetching.current){
			isFetching.current = true;
			fetch(
				SERVER_URL + "/letters/move", 
				{method: "POST", body: JSON.stringify({ids: [letterData.id], folder: slug})}
			).then(res => res.json()).then(res => {
				if(res?.status === 200){
					globalKeyValueEventBus.emit("letterMove", [letterData.id]);
					globalKeyValueEventBus.emit("count_income");
					state(false);
					isFetching.current = false;
				}
			});
		}
	}

	return(
		<div className="letter__context__menu" ref = {contextMenuRef}>
			<a className="letter__context__menu__item letter__context__menu__item__hover" href = {`/${folder}/${letterData.id}/`} target="_blank">
				<span className="letter__context__menu__icon__wrapper">
					<img src = {SERVER_URL + "/static/new_tab.svg"} alt = "" className="letter__context__menu__icon"/>
				</span>
				<span className = "text__primary letter__context__menu__item__text">{FormatMessage("CM_NEW_TAB")}</span>
			</a>

			<hr className="context__menu__deliminter"/>

				<div 
					className="letter__context__menu__item letter__context__menu__item__hover"
					onClick = {e => {MoveToFolder("deleted")}}
				>
					<span className="letter__context__menu__icon__wrapper">
						<img src = {SERVER_URL + "/static/bucket.svg"} alt = "" className="letter__context__menu__icon"/>
					</span>
					<span className = "text__primary letter__context__menu__item__text">{FormatMessage("CM_DELETE")}</span>
				</div>

				<div 
					className="letter__context__menu__item letter__context__menu__item__hover"
					onClick = {e => {MoveToFolder("archive")}}
				>
					<span className="letter__context__menu__icon__wrapper">
						<img src = {SERVER_URL + "/static/archive.svg"} alt = "" className="letter__context__menu__icon"/>
					</span>
					<span className = "text__primary letter__context__menu__item__text">{FormatMessage("CM_ARCHIVE")}</span>
				</div>

				<div 
					className="letter__context__menu__item letter__context__menu__item__hover"
					onClick = {e => {MoveToFolder("spam")}}
				>
					<span className="letter__context__menu__icon__wrapper">
						<img src = {SERVER_URL + "/static/spam.svg"} alt = "" className="letter__context__menu__icon"/>
					</span>
					<span className = "text__primary letter__context__menu__item__text">{FormatMessage("CM_SPAM")}</span>
				</div>

				<div 
					className={`
						letter__context__menu__item 
						letter__context__menu__item__hover 
						letter__context__menu__item__move__to 
						${moveToModalOpen ? "letter__context__menu__item__move__to__active" : ""}
					`}
					onClick = {e => setMoveToModalOpen(!moveToModalOpen)}
					>
					<span 
						className = "letter__context__menu__item__move__to__content"
					>
						<span className = "letter__context__menu__item__move__to__content__label">
							<span className="letter__context__menu__icon__wrapper">
								<img src = {SERVER_URL + "/static/folder.svg"} alt = "" className="letter__context__menu__icon"/>
							</span>
							<span className = "text__primary letter__context__menu__item__text">{FormatMessage("CM_MOVE_TO")}</span>
						</span>
						<span>
							<span className="letter__context__menu__icon__wrapper">
								<img src = {SERVER_URL + "/static/chevron_left.svg"} alt = "" className="letter__context__menu__icon letter__context__menu__chevron"/>
							</span>
						</span>
					</span>
					{moveToModalOpen ? 
						<div className="letter__context__menu__move__to__dropdown" ref = {foldersDropdownRef}>
							{FOLDERS.map(item => {
								if(	item.slug === folder){
									return(
										<div className="letter__context__menu__item letter__context__menu__item__current">
											<span className="letter__context__menu__icon__wrapper">
												<img src = {SERVER_URL + `/static/${item.icon}.svg`} alt = "" className="letter__context__menu__icon"/>
											</span>
											<span className = "text__primary letter__context__menu__item__text">{FormatMessage(item.localeKey)}</span>
										</div>
									)
								}
								else{
									return(
										<div 
											className="letter__context__menu__item letter__context__menu__item__hover"
											onClick = {e => {MoveToFolder(item.slug)}}
										>
											<span className="letter__context__menu__icon__wrapper">
												<img src = {SERVER_URL + `/static/${item.icon}.svg`} alt = "" className="letter__context__menu__icon"/>
											</span>
											<span className = "text__primary letter__context__menu__item__text">{FormatMessage(item.localeKey)}</span>
										</div>
									)
								}
							})}
						</div>
					: null}
				</div>
			<hr className="context__menu__deliminter"/>
				<div 
					className="letter__context__menu__item letter__context__menu__item__hover"
					onClick = {e => {
						if(!isFetching.current){
							isFetching.current = true;
							fetch(SERVER_URL + "/letters/update", {
								body: JSON.stringify({attr: "read", next: !letterData.read, ids: [letterData.id]}),
								method: "POST"
							}).then(res => res.json()).then(res => {
								if(res?.status === 200){
									setLetter(prev => {
										const item = prev.find(item => item.id === letterData.id)
										item.read = !item.read;
										return [...prev];
									})
									globalKeyValueEventBus.emit("count_income");
									state(false);
									isFetching.current = false;
								}
							})
						}
					}}
				>
					<div class = "letter__context__menu__icon__wrapper">
						<div class = {`letter__item__read ${letterData.read ? "letter__item__read__unread" : "letter__item__read__readed"}`}/>
					</div>
					<span className = "text__primary letter__context__menu__item__text">{FormatMessage(letterData.read ? "CM_READ" : "CM_UNREAD")}</span>
				</div>
				<div 
					className="letter__context__menu__item letter__context__menu__item__hover" 
					onClick = {e => {
						if(!isFetching.current){
							isFetching.current = true;
							fetch(SERVER_URL + "/letters/update", {
								body: JSON.stringify({attr: "bookmark", next: !letterData.bookmark, ids: [letterData.id]}),
								method: "POST"
							}).then(res => res.json()).then(res => {
								if(res?.status === 200){
									setLetter(prev => {
										const item = prev.find(item => item.id === letterData.id)
										item.bookmark = !item.bookmark;
										return [...prev];
									})
									state(false);
									isFetching.current = false;
								}
							})
						}
					}}>
					<span className="letter__context__menu__icon__wrapper">
						<img src = {SERVER_URL + `/static/${letterData.bookmark ? "bookmark" : "bookmark_fill"}.svg`} alt = ""/>
					</span>
					<span className = "text__primary letter__context__menu__item__text">{FormatMessage(letterData.bookmark ? "CM_UNFLAG" : "CM_FLAG")}</span>
				</div>
		</div>
	)
}


export {LetterContextMenu}