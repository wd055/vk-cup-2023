import {useState, useEffect, useRef, useContext} from "preact/hooks"
import { route } from "preact-router"
import {SERVER_URL} from "../index"
import {ScrollElementContext, GlobalKeyValueEventBus} from "../context"

import LetterItem from "./LetterItem"
import NoResultsFoundElement from "./NoResultFoundElement"
import {LetterContextMenu} from "./ContextMenu"

import "./letterListElement.scss"



const getQueryStrStartingWith = (search, ...args) => {
	const entries = search.entries();
	const res = [];
	for(const [key, value] of entries) {
		if(args.find(i => key.startsWith(i)) !== undefined) {
			res.push(key + "=" + value);
		}
	  }
	if(res.length > 0){
		return "?" + res.join("&");
	}
	return "";
}



const LETTER_TAKE = 45;
const MAX_LETTERS = LETTER_TAKE * 2;
const LETTER_SIZE = 48;






const LettersListContent = ({folder}) => {
	const [letters, setLetter] = useState(null);
	const [activeLetters, setActiveLetters] = useState([]);

	
	folder = folder === "" ? "mail" : folder;


	const filterQuery = getQueryStrStartingWith(new URLSearchParams(location.search), "filter", "sort");

	const letterListRef = useRef();
	const scrollAble = useContext(ScrollElementContext);
	const globalKeyValueEventBus = useContext(GlobalKeyValueEventBus);

	const [offset, setOffset] = useState(0);
	const [screenItems, setScreenItems] = useState(0);

	const currentGroupId = useRef(0);
	const fetchingData = useRef(false);
	const hasNext = useRef(true);


	const [contextMenuOpened, setContextMenuOpened] = useState(false);
	const [contextMenuLetterData, setContextMenuLetterData]  = useState(null);
	const [contextMenuPos, setContextMenuPos] = useState({x: 0, y: 0});


	const getActiveStateNumber = (idx) => {
		let p = activeLetters[idx - 1]?.active;
		let c = activeLetters[idx]?.active;
		let n = activeLetters[idx + 1]?.active;

	
		if(p && c && n) return 4;
		else if(n && c) return 3;
		else if(p && c) return 2;
		return c ? 1 : 0;
	}

	const FetchData = (direction, willTake, replace) => {

		const fetch_offset = !replace ? (currentGroupId.current > 0 ? MAX_LETTERS + (LETTER_TAKE * currentGroupId.current) : MAX_LETTERS) : 0;


		fetch(SERVER_URL + `/letters/${folder ?? ""}${filterQuery}${filterQuery === "" ? "?" : "&"}take=${willTake ?? LETTER_TAKE}&offset=${fetch_offset}`)
		.then(res => {
			if(res.status === 404){
				route("/")
			}
			else return res.json()
		})
		.then(res => {
			if(!replace && letters?.length > 0){
				if(direction === "next"){
					const newActive = res.map(item => {return {id: item.id, active: false}})
					setLetter(prev => {
						return [...prev, ...res]
					});
					setActiveLetters(prev => [...prev, ...newActive])
				}
			}
			else{
				res = res ?? [];
				setLetter(res);
				const newActive = res.map(item => {return {id: item.id, active: false}})
				setActiveLetters(newActive)
			}
			

			fetchingData.current = false;
			hasNext.current = res.length >= LETTER_TAKE;
		})
	}

	useEffect(() => {


		setScreenItems(Math.floor(scrollAble.current.offsetHeight / LETTER_SIZE));

		globalKeyValueEventBus.subscribe("letter_list", "letterMove", (ids) => {
			if(ids.length > 0){
				setActiveLetters(prev => {
					return [...prev.filter(item => !ids.includes(item.id))];
				})
				setLetter(prev => {
					return [...prev.filter(item => !ids.includes(item.id))];
				})
			}
		})

		const resizeEvent = (e) => {
			const next = Math.floor(scrollAble.current.offsetHeight / LETTER_SIZE);
			if(screenItems !== next){
				setScreenItems(next);
			}
		}
		window.addEventListener("resize", resizeEvent);
		return () => window.removeEventListener("resize", resizeEvent);
		
	}, [])

	
	useEffect(() => {
		setOffset(0);
		scrollAble.current.scrollTo({top: 0})
		currentGroupId.current = 0;

		FetchData(null, MAX_LETTERS, true);
	}, [folder, filterQuery])



	useEffect(() => {
		if(letters && letterListRef.current){
			const height = LETTER_SIZE * letters.length;
			const llr = letterListRef.current.style;
			llr.height = `${height}px`;
			llr.maxHeight = `${height}px`;
			llr.minHeight = `${height}px`;
		}
	}, [letters?.length])


	useEffect(() => {
		if(scrollAble.current){
			const scrollEvent = () => {
				const letters_listed = Math.floor(scrollAble.current.scrollTop / LETTER_SIZE);
				setOffset(letters_listed < 0 ? 0 : letters_listed);
				if(letters && hasNext.current && !fetchingData.current && letters_listed > letters.length - LETTER_TAKE){
					fetchingData.current = true;
					FetchData("next")
					currentGroupId.current += 1;
				}
			}
			scrollAble.current.addEventListener("scroll", scrollEvent)
			return () => scrollAble.current?.removeEventListener("scroll", scrollEvent);
		}
	}, [screenItems, letters])




	const HandleContextMenu = (e, letter) => {
		e.preventDefault();
		setContextMenuLetterData(letter);
		setContextMenuPos({x: e.pageX, y: e.pageY});
		setContextMenuOpened(true)
	}


	useEffect(() => {
		if(contextMenuOpened){
			const scrollEvent = (e) => {
				setContextMenuOpened(false);
				window.removeEventListener("mousedown", clickEvent);
				scrollAble.current?.removeEventListener("scroll", scrollEvent);
			}
			scrollAble.current.addEventListener("scroll", scrollEvent);
			return () => scrollAble.current.removeEventListener("scroll", scrollEvent);
		}
	}, [contextMenuOpened, contextMenuPos])

	if(letters && letters.length > 0){
		const sliceStart = offset - 8 < 0 ? 0 : offset - 8;
		const sliceEnd = (screenItems + 8) + offset;
		return(
			<>
			<div 
				class = "page__content letter__list__desktop letter__list__scroll" 
				ref = {letterListRef}
			>
				{(letters.slice(sliceStart, sliceEnd)).map((letter, i) => {
					const idx = sliceStart + i
					const marginTop = (idx) * LETTER_SIZE;
					const activeState = getActiveStateNumber(idx);

					return (
						<LetterItem 
							data = {letter} 
							folder = {folder} 
							key = {`letter-item-${letter.id}`}
							top = {marginTop}
							setActive = {setActiveLetters}
							active = {activeState}
							activeArray = {activeLetters}
							HandleContextMenu = {HandleContextMenu}
						/>

					)
				})}
			</div>
			{contextMenuOpened ? 
				<LetterContextMenu letterData = {contextMenuLetterData} folder = {folder} state = {setContextMenuOpened} setLetter = {setLetter} pos = {contextMenuPos}/>
				: null
			}
			</>
		)
	}
	else if(letters && letters.length === 0){
		return <NoResultsFoundElement/>
	}
	return null;
}


const LettersList = ({folder}) => {
	return(
		<LettersListContent key = {folder} folder = {folder}/>
	)
}

export default LettersList