export type LetterAttachment = {
  readonly id: number;
  readonly type: 'image'; // TODO: move to enum
  readonly src: string;
  readonly name: string;
  readonly size: {
    readonly value: number;
    readonly power: number;
  };
};
