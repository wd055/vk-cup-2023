import { Letter } from './Letter';

export type LetterListItem = Omit<Letter, 'doc' | 'to'> & {
  readonly attachment: boolean;
};
