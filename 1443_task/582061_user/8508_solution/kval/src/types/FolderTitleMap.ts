import { Folder } from './Folder';

export const FolderTitleMap: { [K in Folder]: string } = {
  [Folder.Inbox]: 'Входящие',
  [Folder.Important]: 'Важное',
  [Folder.Sent]: 'Отправленные',
  [Folder.Drafts]: 'Черновики',
  [Folder.Archive]: 'Архив',
  [Folder.Spam]: 'Спам',
  [Folder.Trash]: 'Корзина',
};

export const FolderTitleReverseMap: { [key: string]: Folder } = Object.entries(
  FolderTitleMap,
).reduce(
  (acc, [key, value]) => ({
    ...acc,
    [value]: key,
  }),
  {},
);
