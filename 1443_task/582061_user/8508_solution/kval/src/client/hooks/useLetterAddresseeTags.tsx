import React from 'react';
import { Letter } from 'types';
import { AddresseeTag } from '../components';

export type UseLetterAddresseeTagsParams = {
  readonly addressees: Letter['to'];
  readonly shouldCrop?: boolean;
  readonly cropAfterCount?: number;
};

export const useLetterAddresseeTags = ({
  addressees,
  cropAfterCount = 3,
  shouldCrop = true,
}: UseLetterAddresseeTagsParams) => {
  const [isCropped, setIsCropped] = React.useState(shouldCrop);

  const list = React.useMemo(
    () =>
      addressees
        .slice(0, isCropped ? cropAfterCount : addressees.length)
        .map(({ name, surname, email }, index) => {
          let fullName = `${name} ${surname}`.trim();
          const isLast = index === 2 || index === addressees.length - 1;

          if (!isLast) {
            fullName += ',';
          }

          return <AddresseeTag key={email} name={fullName} email={email} />;
        }),
    [addressees, isCropped, cropAfterCount],
  );

  const expand = React.useCallback(() => setIsCropped(false), []);

  return {
    list,
    isCropped: list.length !== addressees.length,
    expand,
    croppedCount: addressees.length - list.length,
  };
};
