import React from 'react';
import { Letter, LetterAttachment } from 'types';

import { fetchLetterAttachments } from '../fetchers';

export type UseLetterApiParams = {
  readonly id: Letter['id'];
  readonly folder: string;
};

export const useLetterApi = ({ id, folder }: UseLetterApiParams) => {
  const [attachments, setAttachments] = React.useState<LetterAttachment[]>([]);

  // TODO: add exception handling
  const getAttachments = React.useCallback(async () => {
    try {
      const response = await fetchLetterAttachments({ id, folder });

      console.log(response);

      setAttachments(response.attachments);
    } catch (error) {
      // TODO: return error
    }
  }, [id, folder]);

  return {
    attachments,
    getAttachments,
  };
};
