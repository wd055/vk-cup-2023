import React from 'react';
import { FolderContext } from 'src/client/contexts/FolderContext';
import { fetchLetters } from 'src/client/fetchers';

export const useFolderApi = () => {
  const { folderRef, pageRef, countRef, filtersRef, sortRef } =
    React.useContext(FolderContext);

  const getLetters = React.useCallback(
    async (page: number, limit: number = 50) => {
      if (!folderRef.current) {
        return [];
      }

      const { letters, count } = await fetchLetters({
        page,
        limit,
        folder: folderRef.current,
        filters: filtersRef.current,
        sort: sortRef.current,
      });

      console.log('API', letters.length, count);

      countRef.current = count;
      pageRef.current = page;

      return letters;
    },
    [],
  );

  return {
    getLetters,
  };
};
