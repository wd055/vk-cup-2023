import { I18n } from 'i18n';
import { DateInput, FORMATTER, isToday, isYesterday } from 'date-format';

export function formatLetterDateDetailed(input: DateInput): string {
  let day = '';

  if (isToday(input)) {
    day = I18n.Instance.translate('component.date.today');
  } else if (isYesterday(input)) {
    day = I18n.Instance.translate('component.date.yesterday');
  } else {
    day = FORMATTER.format(input, 'DD MMMM YYYY');
  }

  const time = FORMATTER.format(input, 'hh:mm');

  return `${day}, ${time}`;
}
