import { isToday, isCurrentYear, FORMATTER, DateInput } from 'date-format';

export function formatLetterDate(input: DateInput): string {
  if (isToday(input)) {
    return FORMATTER.format(input, 'hh:mm');
  }

  if (!isCurrentYear(input)) {
    return FORMATTER.format(input, 'DD.MM.YYYY');
  }

  return FORMATTER.format(input, 'DD MMM');
}
