import { I18n } from 'i18n';

export function formatByteSizePower(power: number): string {
  const baseKey = 'domain.attachment.file_size';
  const t = (key: string) => I18n.Instance.translate(`${baseKey}.${key}`);

  switch (power) {
    case 0:
      return t('byte');

    case 10:
      return t('kb');

    case 20:
      return t('mb');

    case 30:
      return t('gb');

    case 40:
      return t('tb');

    default:
      return `x 2^${power} ${t('byte')}`;
  }
}
