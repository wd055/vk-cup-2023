import { Letter } from 'types';
import { API_URL } from './API_URL';

export type FetchLetterParams = {
  readonly id: number;
  readonly folder: string;
};

export async function fetchLetter(
  params: FetchLetterParams,
): Promise<{ letter: Letter }> {
  const { id, folder } = params;

  const response = await fetch(`${API_URL}/api/letters/${folder}/${id}`);
  const json = await response.json();

  return json.data;
}
