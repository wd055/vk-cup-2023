import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { Layout } from 'ui';
import {
  createGetStaticProps,
  createGetStaticPaths,
  useTranslation,
} from 'i18n';

import { useFolders } from 'src/client/hooks';
import { AppHeader, AppSidebar } from '../components';
import { FolderPage } from '../components/pages/FolderPage';
import { LetterPage } from '../components/pages/LetterPage';
import { FolderContextProvider } from '../contexts/FolderContext';

export default function Folder() {
  const { t } = useTranslation();
  const folders = useFolders();
  const routes = React.useMemo(
    () =>
      folders.map((folder) => ({
        ...folder,
        title: t(`domain.folder.${folder.key}`),
        path: `/${folder.key}`,
      })),
    [folders],
  );

  return (
    <FolderContextProvider>
      <Layout header={<AppHeader />} sidebar={<AppSidebar options={routes} />}>
        <Routes>
          <Route path="/:folder" element={<FolderPage />} />
          <Route path="/:folder/:id" element={<LetterPage />} />
        </Routes>
      </Layout>
    </FolderContextProvider>
  );
}

export const getStaticProps = createGetStaticProps((lang) =>
  // eslint-disable-next-line import/no-dynamic-require, global-require
  require(`../public/static/i18n/${lang}.json`),
);
export const getStaticPaths = createGetStaticPaths();
