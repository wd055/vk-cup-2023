import { CustomTheme, SystemTheme } from 'ui/types';

export const SYSTEM_THEMES = [
  {
    theme: SystemTheme.Light,
    preview: 'url("/static/themes/light-preview.jpg")',
  },
  {
    theme: SystemTheme.Dark,
    preview: 'url("/static/themes/dark-preview.jpg")',
  },
];

export const THEMES: {
  readonly colors: CustomTheme[];
  readonly images: CustomTheme[];
} = {
  colors: [
    {
      name: 'brown',
      overrides: {
        '--theme-bg-app': '#4a352f',
        '--theme-bg-header': '#291b18',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'slate-gray',
      overrides: {
        '--theme-bg-app': '#424242',
        '--theme-bg-header': '#242424',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'purple',
      overrides: {
        '--theme-bg-app': '#5a355a',
        '--theme-bg-header': '#331d33',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'slate-blue',
      overrides: {
        '--theme-bg-app': '#35385a',
        '--theme-bg-header': '#171928',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'slate-blue-light',
      overrides: {
        '--theme-bg-app': '#646ecb',
        '--theme-bg-header': '#434c98',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'pink',
      overrides: {
        '--theme-bg-app': '#e73672',
        '--theme-bg-header': '#b31c4f',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'tomato',
      overrides: {
        '--theme-bg-app': '#f44336',
        '--theme-bg-header': '#b73026',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'sea-green',
      overrides: {
        '--theme-bg-app': '#388e3c',
        '--theme-bg-header': '#27662a',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'aquamarine',
      overrides: {
        '--theme-bg-app': '#81d8d0',
        '--theme-bg-header': '#3c928a',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'gainsboro',
      overrides: {
        '--theme-bg-app': '#e2dcd2',
        '--theme-bg-header': '#837e78',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'almond',
      overrides: {
        '--theme-bg-app': '#ffebdc',
        '--theme-bg-header': '#8c847e',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'beige',
      overrides: {
        '--theme-bg-app': '#e7eed2',
        '--theme-bg-header': '#747a63',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'cyan-light',
      overrides: {
        '--theme-bg-app': '#d0f0f7',
        '--theme-bg-header': '#556f74',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'lavender',
      overrides: {
        '--theme-bg-app': '#c9d0fb',
        '--theme-bg-header': '#5a5e76',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'sky',
      overrides: {
        '--theme-bg-app': '#ddf3ff',
        '--theme-bg-header': '#535f6b',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
    {
      name: 'smoke',
      overrides: {
        '--theme-bg-app': '#f0f0f0',
        '--theme-bg-header': '#808080',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Light,
      extendsFrom: SystemTheme.Light,
    },
  ],
  images: [
    // i18n
    {
      name: 'anime',
      overrides: {
        '--theme-bg-app':
          'linear-gradient(180deg, rgba(117, 0, 69, 0.64) 0%, rgba(0, 9, 83, 0.64) 100%), url("/static/themes/anime-background"), #ff0f9d',
        '--theme-bg-header': '#6b1344',
        '--theme-bg-empty-space': 'url("/static/themes/anime-empty-space.svg")',
        '--theme-color-logo-text': '#ffffff',
      },
      contrast: SystemTheme.Dark,
      extendsFrom: SystemTheme.Light,
      preview: 'url("/static/themes/anime-preview.jpg")',
    },
  ],
};
