import React from 'react';
import { useParams } from 'react-router-dom';

import { LetterCard } from '../organisms';

export type LetterPageProps = {};

export const LetterPage: React.FC<LetterPageProps> = () => {
  const { folder, id } = useParams();

  if (!folder || !id || Number.isNaN(Number(id))) {
    // TODO: add error boundaries
    return null;
  }

  return <LetterCard folder={folder} id={Number(id)} />;
};
