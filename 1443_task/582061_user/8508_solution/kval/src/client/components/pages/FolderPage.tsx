import Head from 'next/head';
import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'i18n';

import { LetterList } from '../organisms';

export type FolderPageProps = {};

export const FolderPage: React.FC<FolderPageProps> = () => {
  const { t } = useTranslation();
  const { folder } = useParams();

  if (!folder) {
    // TODO: add error boundaries
    return null;
  }

  return (
    <>
      <Head>
        <title>
          {t(`domain.folder.${folder}`)} - {t('title')}
        </title>
      </Head>
      <LetterList folder={folder} />
    </>
  );
};
