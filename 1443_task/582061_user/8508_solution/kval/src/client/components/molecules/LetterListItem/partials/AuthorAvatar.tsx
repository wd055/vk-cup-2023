import React from 'react';
import clsx from 'clsx';
import { Avatar, Checkbox } from 'ui';

import styles from './AuthorAvatar.module.scss';

export type AuthorAvatarProps = {
  readonly src: string;
  readonly isChecked: boolean;
  readonly onChange: (isChecked: boolean) => void;
  readonly className?: string;
  readonly avatarClassName?: string;
  readonly checkboxClassName?: string;
};

export const AuthorAvatar: React.FC<AuthorAvatarProps> = ({
  src,
  isChecked,
  onChange,
  className,
  avatarClassName,
  checkboxClassName,
}) => (
  <div className={clsx(className, styles.root)}>
    <Avatar className={avatarClassName} src={src} />
    <Checkbox
      className={clsx(checkboxClassName, styles.root__checkbox)}
      checked={isChecked}
      labelProps={{
        onClick: (event) => {
          event.preventDefault();

          onChange(!isChecked);
        },
      }}
      onChange={() => {}}
    />
  </div>
);
