import React from 'react';
import {
  Button,
  Divider,
  Icon,
  Popup,
  Select,
  Space,
  Typography,
  useSelectValue,
} from 'ui';
import {
  IconAttachment,
  IconBookmarkFilled,
  IconChevronDown,
  IconUnread,
} from 'icons';
import { useTranslation } from 'i18n';

import { FolderContext } from 'src/client/contexts/FolderContext';
import { useFolderApi } from 'src/client/hooks';
import { SortPopup } from './partials/SortPopup';

import styles from './FolderFilterButton.module.scss';

export type FolderFilterButtonProps = {};

export const FolderFilterButton: React.FC<FolderFilterButtonProps> = () => {
  const { t } = useTranslation();
  const api = useFolderApi();
  const { filtersRef, sortRef, setEntities } = React.useContext(FolderContext);

  const handleFilterChange = React.useCallback((state: any) => {
    filtersRef.current = state;

    api.getLetters(0).then(setEntities);
  }, []);

  const handleSortChange = React.useCallback((value: string) => {
    sortRef.current = value;

    api.getLetters(0).then(setEntities);
  }, []);

  const { value, reset, handleChange } = useSelectValue({
    onChange: handleFilterChange,
  });

  const options = [
    {
      value: 'read',
      title: t('domain.filter.unread'),
      icon: <Icon color="accent" icon={<IconUnread />} />,
    },
    {
      value: 'bookmark',
      title: t('domain.filter.bookmark'),
      icon: <Icon color="error" icon={<IconBookmarkFilled />} />,
    },
    {
      value: 'doc',
      title: t('domain.filter.attachment'),
      icon: <IconAttachment />,
    },
  ];

  const overlay = (
    <Select
      value={value}
      onChange={handleChange}
      onReset={reset}
      isMultiSelect
      resetOptionText={t('action.reset_all')}
    >
      {options.map(({ value, title, icon }) => (
        <Select.Option key={value} value={value}>
          <Typography className={styles.root__option} preset="body">
            {icon} <span className={styles.root__option_title}>{title}</span>
          </Typography>
        </Select.Option>
      ))}
      <Divider />
      <SortPopup onChange={handleSortChange} />
    </Select>
  );

  const leftIcon = React.useMemo(() => {
    const selected = options.filter((option) => value[option.value]);

    if (selected.length > 0) {
      return (
        <Space size="xs" align="center">
          {selected.map(({ icon }) => icon)}
        </Space>
      );
    }

    return null;
  }, [value, options]);

  return (
    <Popup trigger="click" overlay={overlay} placement="bottom-end">
      <Button
        variant="secondary"
        leftSlot={leftIcon}
        rightSlot={<IconChevronDown />}
        data-theme-contrast="true"
      >
        {t('action.filter')}
      </Button>
    </Popup>
  );
};
