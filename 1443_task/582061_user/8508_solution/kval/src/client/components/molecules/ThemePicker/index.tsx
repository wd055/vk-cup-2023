import React from 'react';
import { Typography, useTheme } from 'ui';
import { useTranslation } from 'i18n';

import { SYSTEM_THEMES, THEMES } from 'src/client/themes';
import { ThemePlate } from '../../atoms';

import styles from './ThemePicker.module.scss';

export type ThemePickerProps = {
  readonly className?: string;
};

export const ThemePicker: React.FC<ThemePickerProps> = () => {
  const { t } = useTranslation();
  const { current, set } = useTheme();

  return (
    <div className={styles.root}>
      <Typography className={styles.root__title} preset="body" component="p">
        {t('component.interface.title')}
      </Typography>
      <div className={styles.root__themes}>
        {THEMES.colors.map((palette) => (
          <div key={palette.name} className={styles.root__palette}>
            <ThemePlate
              isSelected={palette === current}
              background={palette.overrides['--theme-bg-app']}
              onClick={() => set(palette)}
            />
          </div>
        ))}
        {SYSTEM_THEMES.map(({ theme, preview }) => (
          <div key={theme} className={styles.root__theme}>
            <ThemePlate
              size="md"
              isSelected={theme === current}
              background={preview}
              onClick={() => set(theme)}
            />
          </div>
        ))}
        {THEMES.images.map((theme) => (
          <div key={theme.name} className={styles.root__theme}>
            <ThemePlate
              size="md"
              isSelected={theme === current}
              background={theme.preview || theme.overrides['--theme-bg-app']}
              onClick={() => set(theme)}
            />
          </div>
        ))}
      </div>
    </div>
  );
};
