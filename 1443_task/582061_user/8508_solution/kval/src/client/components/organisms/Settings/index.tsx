import React from 'react';
import clsx from 'clsx';
import { Divider, Menu, MenuItem } from 'ui';
import { useTranslation } from 'i18n';

import { LanguagePicker, ThemePicker } from '../../molecules';
import { LanguageFlag } from '../../atoms';

import styles from './Settings.module.scss';

export type SettingsProps = {
  readonly className?: string;
};

enum Tab {
  Interface = 'interface',
  Language = 'language',
}

export const Settings: React.FC<SettingsProps> = ({ className }) => {
  const { t, i18n } = useTranslation();
  const language = i18n.getLanguage();

  // i18n
  const tabs = React.useMemo(
    () => [
      {
        key: Tab.Interface,
        title: t('component.interface.name'),
      },
      {
        key: Tab.Language,
        title: (
          <span className={styles.root__language_title}>
            {t('component.language.name')}: {i18n.getLanguageName(language)}{' '}
            <LanguageFlag language={language} />
          </span>
        ) as React.ReactNode,
      },
    ],
    [language],
  );

  const components = React.useMemo(
    () => ({
      interface: <ThemePicker />,
      language: <LanguagePicker />,
    }),
    [],
  );

  const [tab, setTab] = React.useState(Tab.Interface);

  return (
    <div className={clsx(styles.root, className)}>
      <div className={styles.root__menu}>
        <Menu>
          {tabs.map(({ key, title }) => (
            <MenuItem
              isActive={key === tab}
              key={key}
              title={title}
              icon={null}
              onClick={() => setTab(key)}
            />
          ))}
        </Menu>
      </div>
      <Divider direction="vertical" />
      <div className={styles.root__content}>
        <div key={tab} className={styles.fade_in}>
          {components[tab]}
        </div>
      </div>
    </div>
  );
};
