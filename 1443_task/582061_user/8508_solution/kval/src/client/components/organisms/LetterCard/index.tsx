import React from 'react';
import { Letter } from 'types';
import { Typography, Paper, Loader } from 'ui';

import { fetchLetter } from 'src/client/fetchers';
import { Header } from './partials/Header';
import { Subheader } from './partials/Subheader';
import { Attachments } from './partials/Attachments';

import styles from './LetterCard.module.scss';

export type LetterCardProps = {
  readonly id: number;
  readonly folder: string;
};

export const LetterCard: React.FC<LetterCardProps> = ({ id, folder }) => {
  const [letter, setLetter] = React.useState<Letter | null>(null);

  React.useEffect(() => {
    fetchLetter({ id, folder }).then(({ letter }) => setLetter(letter));
  }, [id]);

  if (!letter) {
    return <Loader isStretched />;
  }

  const { title, flag } = letter;

  return (
    <Paper className={styles.root}>
      <Header className={styles.root__header} title={title} flag={flag} />
      <Subheader className={styles.root__subheader} {...letter} />
      {letter.doc?.img && (
        <Attachments
          className={styles.root__attachments}
          attachments={[
            {
              id: 0,
              type: 'image',
              name: 'file.png',
              size: {
                value: 1.54,
                power: 20,
              },
              src: letter.doc.img,
            },
          ]}
        />
      )}
      <Typography className={styles.root__text} preset="button" component="p">
        {letter.text}
      </Typography>
    </Paper>
  );
};
