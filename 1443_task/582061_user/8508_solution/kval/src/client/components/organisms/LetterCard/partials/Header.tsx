import React from 'react';
import { LetterFlag } from 'types';
import { Space, Typography } from 'ui';
import { useTranslation } from 'i18n';

import { useLetterCategories } from 'src/client/hooks';

export type HeaderProps = {
  readonly title: string;
  readonly flag?: LetterFlag;
  readonly className?: string;
};

export const Header: React.FC<HeaderProps> = ({ title, flag, className }) => {
  const { t } = useTranslation();
  const categories = useLetterCategories({
    flag,
    hasAttachment: false,
  });

  return (
    <Space className={className} size="md" justify="space-between">
      <Typography preset="h1" component="h1">
        {title}
      </Typography>
      <Space>
        {categories.map(({ key, Icon }) => (
          <Space>
            <Icon width="20px" height="20px" />
            <Typography preset="hint">
              {t(`domain.letter.category.${key}`)}
            </Typography>
          </Space>
        ))}
      </Space>
    </Space>
  );
};
