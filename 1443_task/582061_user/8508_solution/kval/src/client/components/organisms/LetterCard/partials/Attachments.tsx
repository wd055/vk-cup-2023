import React from 'react';
import { LetterAttachment } from 'types';
import { Button, Space, Typography } from 'ui';
import { useTranslation } from 'i18n';

import { Attachment } from 'src/client/components';
import { formatByteSizePower, pluralize } from 'src/client/helpers';

import styles from './Attachments.module.scss';

export type AttachmentsProps = {
  readonly attachments: LetterAttachment[];
  readonly className?: string;
};

export const Attachments: React.FC<AttachmentsProps> = ({
  attachments,
  className,
}) => {
  const { t } = useTranslation();

  return (
    <div className={className}>
      <Space className={styles.root__attachments}>
        {attachments.map((attachment) => (
          <Attachment key={attachment.id} {...attachment} />
        ))}
      </Space>
      <Space size="md">
        <Typography preset="footnote">
          {attachments.length}{' '}
          {pluralize(attachments.length, [
            t('domain.attachment.file.base'),
            t('domain.attachment.file.form_1'),
            t('domain.attachment.file.form_2'),
          ])}
        </Typography>
        <Typography preset="footnote" color="secondary">
          <Button variant="text">{t('action.download')}</Button>{' '}
          {/* TODO: calculate total attachments size */}(
          {attachments[0].size.value}{' '}
          {formatByteSizePower(attachments[0].size.power)})
        </Typography>
      </Space>
    </div>
  );
};
