import React from 'react';
import { Header } from 'ui';
import { IconLogo } from 'icons';

import { FolderFilterButton } from '../../molecules';

import styles from './AppHeader.module.scss';

export type AppHeaderProps = {};

export const AppHeader: React.FC<AppHeaderProps> = () => (
  <Header
    leftSlot={<IconLogo iconClassName={styles.root__logo} size="unset" />}
    rightSlot={<FolderFilterButton />}
  />
);
