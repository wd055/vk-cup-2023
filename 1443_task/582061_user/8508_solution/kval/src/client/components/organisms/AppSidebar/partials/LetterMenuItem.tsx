import clsx from 'clsx';
import React from 'react';
import { MenuItem, MenuItemProps } from 'ui';
import { IconEdit } from 'icons';
import { useTranslation } from 'i18n';

import styles from './LetterMenuItem.module.scss';

export type LetterMenuItemProps = Pick<MenuItemProps, 'isCollapsed'>;

export const LetterMenuItem: React.FC<LetterMenuItemProps> = ({
  isCollapsed,
}) => {
  const { t } = useTranslation();

  return (
    <MenuItem
      className={styles.root}
      variant="primary"
      title={t('domain.action.new_letter')}
      isCollapsed={isCollapsed}
      hasTooltip={false}
      icon={
        <IconEdit
          className={clsx(styles.root__icon, isCollapsed && styles.visible)}
        />
      }
    />
  );
};
