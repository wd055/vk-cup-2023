import React, { HTMLAttributes } from 'react';
import clsx from 'clsx';
import { ButtonBase } from 'ui';
import { IconCheckmark } from 'icons';

import styles from './ThemePlate.module.scss';

export type ThemePlateProps = {
  readonly background: string;
  readonly isSelected?: boolean;
  readonly size?: 'sm' | 'md';
  readonly className?: string;
} & HTMLAttributes<HTMLButtonElement>;

export const ThemePlate: React.FC<ThemePlateProps> = ({
  background,
  isSelected,
  className,
  size = 'sm',
  ...rest
}) => (
  <ButtonBase
    {...rest}
    className={clsx(
      styles.root,
      styles[size],
      isSelected && styles.selected,
      className,
    )}
    style={{ background }}
  >
    <IconCheckmark className={styles.root__checkmark} />
  </ButtonBase>
);
