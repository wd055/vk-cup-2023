import React from 'react';
import clsx from 'clsx';
import { Language } from 'types';

import styles from './LanguageFlag.module.scss';

const MAP = {
  [Language.Ru]: '/static/i18n/ru.png',
  [Language.En]: '/static/i18n/eng.png',
};

export type LanguageFlagProps = {
  readonly language: Language;
  readonly className?: string;
};

export const LanguageFlag: React.FC<LanguageFlagProps> = ({
  language,
  className,
}) => (
  <span
    className={clsx(styles.root, className)}
    style={{ background: `url(${MAP[language]})` }}
  />
);
