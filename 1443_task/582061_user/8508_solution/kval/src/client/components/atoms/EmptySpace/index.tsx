import React from 'react';
import { Typography } from 'ui';
import { useTranslation } from 'i18n';

import styles from './EmptySpace.module.scss';

export type EmptySpaceProps = {};

export const EmptySpace: React.FC<EmptySpaceProps> = () => {
  const { t } = useTranslation();

  return (
    <div className={styles.root}>
      <div className={styles.root__image} />
      <Typography preset="h1" isBold data-theme-contrast="true">
        {t('component.empty_space.title')}
      </Typography>
    </div>
  );
};
