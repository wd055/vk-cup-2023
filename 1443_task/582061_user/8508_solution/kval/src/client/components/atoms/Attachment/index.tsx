import React from 'react';
import clsx from 'clsx';
import { Button, Image } from 'ui';
import { LetterAttachment } from 'types';
import { IconDownload } from 'icons';
import { useTranslation } from 'i18n';

import styles from './Attachment.module.scss';

export type AttachmentProps = LetterAttachment & {
  readonly className?: string;
};

export const Attachment: React.FC<AttachmentProps> = ({
  src,
  name,
  className,
}) => {
  const { t } = useTranslation();

  return (
    <div className={clsx(styles.root, className)}>
      <Image src={src} alt={name} width={256} height={190} />
      <div className={styles.root__download}>
        <Button variant="secondary" leftSlot={<IconDownload />}>
          {t('action.download')}
        </Button>
      </div>
    </div>
  );
};
