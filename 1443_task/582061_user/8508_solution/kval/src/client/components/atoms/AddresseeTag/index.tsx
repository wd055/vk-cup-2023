import React from 'react';
import { Tooltip, Typography } from 'ui';

import styles from './AddresseeTag.module.scss';

export type AddresseeTagProps = {
  readonly name: string;
  readonly email: string;
};

export const AddresseeTag: React.FC<AddresseeTagProps> = ({ name, email }) => (
  <Tooltip overlay={email} mouseEnterDelay={0.3}>
    <Typography className={styles.root} preset="footnote" color="secondary">
      {name}
    </Typography>
  </Tooltip>
);
