import React, { SetStateAction } from 'react';
import { LetterListItem } from 'types';

import { FolderFilters } from '../fetchers';

export type FolderContextState = {
  readonly entities: LetterListItem[];
  readonly isLoading: boolean;
  readonly folderRef: React.MutableRefObject<string>;
  readonly pageRef: React.MutableRefObject<number>;
  readonly countRef: React.MutableRefObject<number>;
  readonly scrollRef: React.MutableRefObject<number>;

  readonly filtersRef: React.MutableRefObject<FolderFilters>;
  readonly sortRef: React.MutableRefObject<string>;

  readonly setEntities: React.Dispatch<SetStateAction<LetterListItem[]>>;
  readonly setIsLoading: React.Dispatch<SetStateAction<boolean>>;
};

const defaultValue: FolderContextState = {
  entities: [],
  isLoading: false,
  folderRef: { current: '' },
  pageRef: { current: 0 },
  countRef: { current: 0 },
  scrollRef: { current: 0 },

  filtersRef: {
    current: {
      unread: false,
      bookmark: false,
      attachment: false,
    },
  },
  sortRef: { current: '' },

  setEntities: () => {},
  setIsLoading: () => {},
};

export const FolderContext =
  React.createContext<FolderContextState>(defaultValue);

export const FolderContextProvider: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  const [entities, setEntities] = React.useState<LetterListItem[]>([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const filtersRef = React.useRef(defaultValue.filtersRef.current);
  const sortRef = React.useRef('newest');
  const folderRef = React.useRef('');
  const pageRef = React.useRef(-1);
  const countRef = React.useRef(1);
  const scrollRef = React.useRef(0);

  const value = React.useMemo(
    () => ({
      entities,
      isLoading,
      folderRef,
      pageRef,
      countRef,
      scrollRef,
      filtersRef,
      sortRef,
      setEntities,
      setIsLoading,
    }),
    [isLoading, entities],
  );

  return (
    <FolderContext.Provider value={value}>{children}</FolderContext.Provider>
  );
};
