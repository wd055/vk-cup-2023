import React from 'react';
import clsx from 'clsx';
import { ScrollEvent } from 'scroll-controller';

import { VirtualListController } from '../classes';
import { ViewportItem, VirtualListEvent } from '../types';
import { Item } from './Item';

import styles from './VirtualList.module.scss';

export type VirtualListItem = {
  readonly id: string | number;
};

export type VirtualListProps<T extends VirtualListItem> = {
  readonly height: number;
  readonly itemHeight: number;
  readonly batchSize: number;
  readonly items: T[];
  readonly viewportClassName?: string;
  readonly contentClassName?: string;
  readonly contentBottomSlot?: React.ReactNode;
  readonly renderItem: (item: T) => JSX.Element;
  readonly onScrollEnd: () => void;
};

export function VirtualList<T extends VirtualListItem>(
  props: VirtualListProps<T>,
): JSX.Element {
  const {
    height,
    itemHeight,
    batchSize,
    items,
    viewportClassName,
    contentClassName,
    contentBottomSlot,
    renderItem,
    onScrollEnd,
  } = props;

  const controllerRef = React.useRef(new VirtualListController());
  const viewportRef = React.useRef<HTMLDivElement>(null);
  const contentRef = React.useRef<HTMLDivElement>(null);

  const [viewportItems, setViewportItems] = React.useState<ViewportItem[]>([]);

  React.useEffect(() => {
    const viewportNode = viewportRef.current;
    const contentNode = contentRef.current;

    if (!viewportNode || !contentNode) {
      return;
    }

    const controller = controllerRef.current;

    controller.init({
      viewportNode,
      contentNode,
      height,
      itemHeight,
      batchSize,
      itemsCount: items.length,
    });

    controller.subscribe(VirtualListEvent.OnScroll, setViewportItems);

    return () =>
      controller.unsubscribe(VirtualListEvent.OnScroll, setViewportItems);
  }, [height, itemHeight, batchSize]);

  React.useEffect(() => {
    controllerRef.current.setItemsCount(items.length);
  }, [items.length]);

  React.useEffect(() => {
    controllerRef.current.scroll.subscribe(ScrollEvent.End, onScrollEnd);

    return () =>
      controllerRef.current.scroll.unsubscribe(ScrollEvent.End, onScrollEnd);
  }, [onScrollEnd]);

  React.useEffect(() => {
    controllerRef.current.sync();
  }, [contentBottomSlot]);

  return (
    <div ref={viewportRef} className={viewportClassName}>
      <div
        ref={contentRef}
        className={clsx(contentClassName, styles.root__content)}
      >
        {viewportItems.map(({ index, offsetTop }) => {
          const item = items[index];

          return (
            <Item key={index} top={offsetTop} index={index} id={item?.id}>
              {item && renderItem(item)}
            </Item>
          );
        })}
        {contentBottomSlot && (
          <div className={styles.root__bottom_slot}>{contentBottomSlot}</div>
        )}
      </div>
    </div>
  );
}
