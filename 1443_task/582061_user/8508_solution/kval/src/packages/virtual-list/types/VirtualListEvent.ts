export enum VirtualListEvent {
  OnScroll = 'onscroll',
  OnEnd = 'onend',
}
