import { DateInput } from '../types';

export function isDateSame(
  input1: DateInput,
  input2: DateInput,
  ...inputs: DateInput[]
): boolean {
  const date = new Date(input1).toDateString();
  const dates = [input2, ...inputs].map((d) => new Date(d));

  return dates.every((d) => d.toDateString() === date);
}
