export type PatternReplacer = (date: Date, locale: string) => string;
