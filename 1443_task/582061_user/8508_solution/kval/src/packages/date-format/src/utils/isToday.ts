import { DateInput } from '../types';
import { isDateSame } from './isDateSame';

export function isToday(input: DateInput): boolean {
  return isDateSame(input, new Date());
}
