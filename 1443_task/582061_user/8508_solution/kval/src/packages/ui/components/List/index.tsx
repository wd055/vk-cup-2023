import React from 'react';
import clsx from 'clsx';

import styles from './List.module.scss';

export type ListProps = React.PropsWithChildren<{
  readonly className?: string;
  readonly onEndReach?: () => void;
}>;

export const List: React.FC<ListProps> = ({
  className,
  children,
  onEndReach,
}) => {
  const nodeRef = React.useRef<HTMLElement>(null);

  React.useEffect(() => {
    if (!nodeRef.current || !onEndReach) {
      return;
    }

    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.intersectionRatio === 1) {
            onEndReach();
          }
        });
      },
      {
        threshold: 0.1,
      },
    );

    observer.observe(nodeRef.current);

    return () => observer.disconnect();
  }, [onEndReach]);

  return (
    <ul className={clsx(className, styles.root)}>
      {children}
      <span ref={nodeRef} />
    </ul>
  );
};
