import React, { HTMLAttributes } from 'react';
import clsx from 'clsx';

import { Button, ButtonProps } from '../Button';
import { Tooltip } from '../Tooltip';

import styles from './MenuItem.module.scss';

export type MenuItemProps = {
  readonly title: React.ReactNode;
  readonly icon: React.ReactNode;
  readonly isActive?: boolean;
  readonly isCollapsed?: boolean;
  readonly hasTooltip?: boolean;
  readonly tooltipTitle?: string;
} & Omit<ButtonProps & HTMLAttributes<HTMLButtonElement>, 'title'>;

export const MenuItem: React.FC<MenuItemProps> = ({
  isActive,
  title,
  icon,
  isCollapsed,
  className,
  tooltipTitle,
  hasTooltip = true,
  ...rest
}) => {
  const content = (
    <Button
      variant="secondary"
      {...rest}
      className={clsx(
        className,
        styles.root,
        isActive && styles.active,
        isCollapsed && styles.collapsed,
      )}
      contentClassName={styles.root__title}
      leftSlot={icon}
      isStretched
    >
      {title}
    </Button>
  );

  if (hasTooltip) {
    return (
      <Tooltip overlay={tooltipTitle || title} placement="right">
        {content}
      </Tooltip>
    );
  }

  return content;
};
