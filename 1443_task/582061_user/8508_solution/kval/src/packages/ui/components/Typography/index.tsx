import clsx from 'clsx';
import React from 'react';

import styles from './Typography.module.scss';

export type TypographyProps = React.PropsWithChildren<{
  readonly preset: 'h1' | 'body' | 'footnote' | 'button' | 'hint';
  readonly color?: 'primary' | 'secondary' | 'ghost' | 'link';
  readonly className?: string;
  readonly isBold?: boolean;
  readonly shouldCrop?: boolean;
}>;

// TODO: show tooltip on hover if text is overflowed
export const Typography: Utils.FCWithComponentProp<TypographyProps, 'span'> = ({
  preset,
  className,
  isBold,
  shouldCrop,
  children,
  color = 'primary',
  component: Component = 'span',
  ...rest
}) => (
  <Component
    {...rest}
    className={clsx(
      className,
      styles.root,
      styles[preset],
      styles[color],
      isBold && styles.bold,
      shouldCrop && styles.crop,
    )}
  >
    {children}
  </Component>
);
