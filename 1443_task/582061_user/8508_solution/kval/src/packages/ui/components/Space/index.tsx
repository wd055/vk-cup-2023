import React from 'react';
import clsx from 'clsx';

import styles from './Space.module.scss';

export type SpaceProps = React.PropsWithChildren<{
  readonly className?: string;
  readonly size?: 'xs' | 'sm' | 'md';
  readonly justify?: React.CSSProperties['justifyContent'];
  readonly align?: React.CSSProperties['alignItems'];
}>;

export const Space: Utils.FCWithComponentProp<SpaceProps, 'div'> = ({
  className,
  children,
  justify,
  align = 'center',
  size = 'sm',
  component: Component = 'div',
}) => (
  <Component
    className={clsx(className, styles.root, styles[size])}
    style={{ justifyContent: justify, alignItems: align }}
  >
    {children}
  </Component>
);
