import React, { InputHTMLAttributes, LabelHTMLAttributes } from 'react';
import clsx from 'clsx';
import { nanoid } from 'nanoid';

import styles from './Radio.module.scss';

export type RadioProps = {
  readonly labelProps?: LabelHTMLAttributes<HTMLLabelElement>;
} & Omit<InputHTMLAttributes<HTMLInputElement>, 'type'>;

export const Radio: React.FC<RadioProps> = ({
  className,
  labelProps,
  id: idFromProps,
  children,
  ...rest
}) => {
  const id = React.useMemo(() => idFromProps || nanoid(), [idFromProps]);

  return (
    <label
      {...labelProps}
      htmlFor={id}
      className={clsx(className, styles.root, rest.checked && styles.checked)}
    >
      <input {...rest} id={id} type="radio" className={styles.root__input} />
      <div className={styles.root__border} />
      {children}
    </label>
  );
};
