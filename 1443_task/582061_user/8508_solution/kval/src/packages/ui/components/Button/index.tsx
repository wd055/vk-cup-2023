import clsx from 'clsx';
import React from 'react';

import { ButtonBase, ButtonBaseProps } from '../ButtonBase';

import styles from './Button.module.scss';

export type ButtonVariant = 'primary' | 'secondary' | 'ghost' | 'text';

export type ButtonProps = ButtonBaseProps & {
  readonly leftSlot?: React.ReactNode;
  readonly rightSlot?: React.ReactNode;
  readonly contentClassName?: string;
  readonly isStretched?: boolean;
  readonly variant?: ButtonVariant;
  readonly isIconOnly?: boolean;
};

export const Button: Utils.FCWithComponentProp<ButtonProps, 'button'> = ({
  leftSlot,
  rightSlot,
  children,
  className,
  contentClassName,
  isStretched,
  isCentered,
  isIconOnly,
  variant = 'primary',
  ...rest
}) => (
  <ButtonBase
    {...rest}
    className={clsx(
      className,
      styles.root,
      styles[variant],
      isStretched && styles.stretch,
    )}
  >
    {leftSlot && <span className={styles.root__left}>{leftSlot}</span>}
    <span
      className={clsx(
        contentClassName,
        styles.root__content,
        isIconOnly && styles.icon_only,
        Boolean(leftSlot) && styles.pad_left,
      )}
    >
      {children}
    </span>
    {rightSlot && <span className={styles.root__right}>{rightSlot}</span>}
  </ButtonBase>
);
