import React from 'react';
import clsx from 'clsx';

import styles from './Header.module.scss';

export type HeaderProps = React.PropsWithChildren<{
  readonly className?: string;
  readonly leftSlot?: React.ReactNode;
  readonly rightSlot?: React.ReactNode;
}>;

export const Header: React.FC<HeaderProps> = ({
  className,
  leftSlot,
  rightSlot,
  children,
}) => (
  <header className={clsx(className, styles.root)}>
    {leftSlot}
    {children}
    {rightSlot}
  </header>
);
