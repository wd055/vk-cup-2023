import React from 'react';
import clsx from 'clsx';
import { IconCheckmark } from 'icons';

import { SelectContext } from '../SelectContext';
import { ListItem } from '../../ListItem';

import styles from './Option.module.scss';

export type OptionProps = {
  readonly value: string;
  readonly className?: string;
};

export const Option: React.FC<React.PropsWithChildren<OptionProps>> = ({
  value,
  className,
  children,
}) => {
  const { state, isMultiSelect, setState } = React.useContext(SelectContext);

  const handleClick = React.useCallback(() => {
    const baseState = isMultiSelect
      ? state
      : Object.keys(state).reduce(
          (acc, key) => ({
            ...acc,
            [key]: false,
          }),
          {},
        );

    setState({
      ...baseState,
      [value]: !state[value],
    });
  }, [value, state, isMultiSelect]);

  const isSelected = state[value] || false;

  return (
    <ListItem
      className={clsx(styles.root, isSelected && styles.selected, className)}
      isHoverable
      onClick={handleClick}
    >
      <span className={clsx(styles.root__icon)}>
        <IconCheckmark />
      </span>
      {children}
    </ListItem>
  );
};
