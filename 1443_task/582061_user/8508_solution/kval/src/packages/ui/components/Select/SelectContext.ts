import React from 'react';

export type SelectValueState = {
  readonly [key: string]: boolean;
};

export type SelectContextState = {
  readonly state: SelectValueState;
  readonly isMultiSelect: boolean;

  readonly setState: (state: SelectValueState) => void;
};

export const SelectContext = React.createContext<SelectContextState>({
  state: {},
  isMultiSelect: false,
  setState: () => {},
});
