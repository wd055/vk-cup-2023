import React, { HTMLAttributes } from 'react';
import clsx from 'clsx';

import styles from './Sidebar.module.scss';

export type SidebarProps = React.PropsWithChildren<{
  readonly className?: string;
}> &
  HTMLAttributes<HTMLDivElement>;

export const Sidebar: React.FC<SidebarProps> = ({
  className,
  children,
  ...rest
}) => (
  <aside {...rest} className={clsx(className, styles.root)}>
    {children}
  </aside>
);
