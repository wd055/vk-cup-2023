import React from 'react';
import clsx from 'clsx';

import styles from './ListItem.module.scss';

export type ListItemProps = React.PropsWithChildren<{
  readonly className?: string;
  readonly isHoverable?: boolean;
}>;

export const ListItem: Utils.FCWithComponentProp<ListItemProps, 'li'> = ({
  children,
  className,
  isHoverable,
  component: Component = 'li',
  ...rest
}) => (
  <Component
    {...rest}
    className={clsx(styles.root, isHoverable && styles.hoverable, className)}
  >
    {children}
  </Component>
);
