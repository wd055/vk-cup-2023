import React from 'react';
import { usePopper, PopperProps } from 'react-popper';

export type UsePopoverParams = {
  readonly placement?: PopperProps<any>['placement'];
  readonly offset?: number;
  readonly onOpenChange?: (isOpened: boolean) => void;
};

export const usePopover = (params: UsePopoverParams) => {
  const { placement, offset, onOpenChange } = params;

  const [isMounted, setIsMounted] = React.useState(false);
  const [isOpened, setIsOpened] = React.useState(false);
  const [overlayNode, setOverlayNode] = React.useState<HTMLElement | null>(
    null,
  );
  const [anchorNode, setAnchorNode] = React.useState<HTMLElement | null>(null);

  const { styles, attributes } = usePopper(anchorNode, overlayNode, {
    placement,
    strategy: 'fixed',
    modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, offset],
        },
      },
    ],
  });

  const show = React.useCallback(() => {
    setIsMounted(true);
    setIsOpened(true);
  }, []);

  const hide = React.useCallback(() => {
    setIsOpened(false);
  }, []);

  const handleAnimationEnd = React.useCallback(
    (event: React.AnimationEvent<HTMLDivElement>) => {
      const target = event.target as HTMLDivElement;

      if (!target) {
        setIsMounted(false);

        return;
      }

      const isOpened = target.getAttribute('data-opened') === 'true';

      if (!isOpened) {
        target.style.display = 'none';
        setIsMounted(false);
      }
    },
    [],
  );

  React.useEffect(() => {
    if (onOpenChange) {
      onOpenChange(isOpened);
    }
  }, [isOpened]);

  return {
    isMounted,
    isOpened,
    overlayNode,
    anchorNode,
    popoverStyles: styles.popper,
    popoverAttributes: attributes.popper,

    show,
    hide,

    setIsMounted,
    setIsOpened,
    setOverlayNode,
    setAnchorNode,

    handleAnimationEnd,
  };
};
