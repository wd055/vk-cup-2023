import React from 'react';
import { MenuContext } from '../../contexts/menu-context';

export type MenuProps = React.PropsWithChildren<{
  readonly isCollapsed?: boolean;
}>;

export const Menu: React.FC<MenuProps> = ({ isCollapsed, children }) => {
  const contextValue = React.useMemo(
    () => ({ isCollapsed: isCollapsed || false }),
    [isCollapsed],
  );

  return (
    <MenuContext.Provider value={contextValue}>{children}</MenuContext.Provider>
  );
};
