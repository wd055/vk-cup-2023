import React from 'react';

export type CheckmarkProps = {
  readonly className?: string;
};

export const Checkmark: React.FC<CheckmarkProps> = ({ className }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="8"
    height="6"
    viewBox="0 0 8 6"
    fill="none"
  >
    <path
      fill="currentColor"
      fillRule="evenodd"
      d="M7.7.3c-.4-.4-1-.4-1.4 0L3 3.6 1.7 2.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l2 2c.2.2.4.3.7.3.3 0 .5-.1.7-.3l4-4c.4-.4.4-1 0-1.4Z"
      clipRule="evenodd"
    />
  </svg>
);
