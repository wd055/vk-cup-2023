import React from 'react';
import clsx from 'clsx';

import styles from './Divider.module.scss';

export type DividerProps = {
  readonly className?: string;
  readonly direction?: 'horizontal' | 'vertical';
};

export const Divider: React.FC<DividerProps> = ({
  className,
  direction = 'horizontal',
}) => <div className={clsx(className, styles.root, styles[direction])} />;
