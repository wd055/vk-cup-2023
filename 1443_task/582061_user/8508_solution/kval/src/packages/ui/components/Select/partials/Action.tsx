import React, { HTMLAttributes } from 'react';

import { ListItem } from '../../ListItem';
import { Typography } from '../../Typography';

import styles from './Action.module.scss';

export type ActionProps = HTMLAttributes<HTMLLIElement> & {
  readonly rightSlot?: React.ReactNode;
};

export const Action: React.FC<ActionProps> = ({
  children,
  rightSlot,
  ...rest
}) => (
  <ListItem {...rest} className={styles.root} isHoverable>
    <Typography className={styles.root__text} preset="button">
      {children}

      {rightSlot && <span className={styles.root__right}>{rightSlot}</span>}
    </Typography>
  </ListItem>
);
