import React from 'react';
import ReactDOM from 'react-dom';
import clsx from 'clsx';
import { useTrigger, TriggerKey, mergeEventHandlers } from 'trigger';

import { usePopoverContainer, usePopoverStackContext } from '../../hooks';
import { usePopover, UsePopoverParams } from './usePopover';

import styles from './Popover.module.scss';

export type PopoverProps = UsePopoverParams & {
  readonly trigger: TriggerKey;
  readonly children: React.ReactElement;
  readonly overlay: React.ReactNode;
  readonly events?: React.CSSProperties['pointerEvents'];
  readonly mouseEnterDelay?: number;
  readonly className?: string;
};

export const Popover: React.FC<PopoverProps> = ({
  trigger,
  children,
  overlay,
  placement,
  events,
  offset = 4,
  mouseEnterDelay = 0.5,
  className: classNameFromProps,
  onOpenChange,
  ...rest
}) => {
  const {
    isMounted,
    isOpened,
    anchorNode,
    popoverStyles,
    popoverAttributes,
    show,
    hide,
    setOverlayNode,
    setAnchorNode,
    handleAnimationEnd,
  } = usePopover({ placement, offset, onOpenChange });

  const context = usePopoverStackContext();
  const { container } = usePopoverContainer(isMounted, context.container);

  const { handlers } = useTrigger({
    trigger,
    anchorNode,
    containerNode: container,
    options: { mouseEnterDelay },
    onAnchorSet: setAnchorNode,
    onStateChange: (isOpened) => (isOpened ? show() : hide()),
  });

  React.useEffect(() => {
    if (!context.isOpened) {
      hide();
    }
  }, [context.isOpened]);

  const child = React.cloneElement(
    children,
    mergeEventHandlers(rest, children.props, handlers),
  );

  const className = clsx(
    styles.root,
    isOpened ? styles.opened : styles.closed,
    !isMounted && styles.closed_end,
    classNameFromProps,
  );
  const containerStyles: React.CSSProperties = {
    ...popoverStyles,
    pointerEvents: events,
    zIndex: context.zIndex,
  };

  const content = isMounted ? (
    <div ref={setOverlayNode} style={containerStyles} {...popoverAttributes}>
      <div
        className={className}
        onAnimationEnd={handleAnimationEnd}
        data-opened={isOpened}
      >
        {overlay}
      </div>
    </div>
  ) : (
    // HACK: not using `null` because in some cases Preact crashes render. Possible issue: https://github.com/preactjs/preact-cli/issues/1293
    <div />
  );

  const Wrapper = context.Wrapper;

  return (
    <Wrapper
      container={container}
      zIndex={context.zIndex + 1}
      isOpened={isOpened}
    >
      {child}
      {container && ReactDOM.createPortal(content, container)}
    </Wrapper>
  );
};
