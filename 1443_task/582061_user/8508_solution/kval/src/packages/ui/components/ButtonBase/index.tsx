import React from 'react';

export type ButtonBaseProps = {
  readonly className?: string;
  readonly component?: 'a' | 'button';
};

// TODO: add ref forwarding
export const ButtonBase: Utils.FCWithComponentProp<
  ButtonBaseProps,
  'button'
> = ({ children, component = 'button', ...rest }) =>
  React.createElement(component, rest, children);
