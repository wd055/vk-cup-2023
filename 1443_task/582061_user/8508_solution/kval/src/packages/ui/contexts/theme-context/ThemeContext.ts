import React from 'react';
import { SystemTheme, CustomTheme, Theme } from '../../types';

export type ThemeContextState = {
  readonly current: Theme;
  readonly set: (theme: Theme | CustomTheme) => void;
};

export const ThemeContext = React.createContext<ThemeContextState>({
  current: SystemTheme.Light,
  set: () => {},
});
