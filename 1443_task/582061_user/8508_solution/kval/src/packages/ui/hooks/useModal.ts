import React from 'react';

export const useModal = () => {
  const [isOpened, setIsOpened] = React.useState(false);

  const handleOpen = React.useCallback(() => setIsOpened(true), []);
  const handleClose = React.useCallback(() => setIsOpened(false), []);

  const props = React.useMemo(
    () => ({
      isOpened,
      onClose: handleClose,
    }),
    [isOpened, handleClose],
  );

  return {
    isOpened,
    props,
    open: handleOpen,
    close: handleClose,
  };
};
