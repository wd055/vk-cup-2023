import React from 'react';

import { PopoverStackContext } from '../contexts/PopoverStackContext';

type PopoverWrapper = React.FC<
  React.PropsWithChildren<{
    readonly container: HTMLElement | null;
    readonly zIndex: number;
    readonly isOpened: boolean;
  }>
>;

const PopoverContextWrapper: PopoverWrapper = ({
  children,
  zIndex,
  container,
  isOpened,
}) => {
  const value = React.useMemo(
    () => ({ container, zIndex, isOpened }),
    [container, zIndex, isOpened],
  );

  return (
    <PopoverStackContext.Provider value={value}>
      {children}
    </PopoverStackContext.Provider>
  );
};

export type UsePopoverStackContextParams = {};

export const usePopoverStackContext = () => {
  const { container, zIndex, isOpened } = React.useContext(PopoverStackContext);

  return {
    zIndex,
    container,
    isOpened,
    Wrapper: PopoverContextWrapper,
  };
};
