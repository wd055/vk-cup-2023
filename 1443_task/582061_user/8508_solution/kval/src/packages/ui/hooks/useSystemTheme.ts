import React from 'react';
import { SystemTheme } from '../types';
import { getCurrentSystemTheme, matchSystemThemeMedia } from '../helpers';

export type UseSystemThemeParams = {
  readonly onThemeChange: (theme: SystemTheme) => void;
};

export const useSystemTheme = (params: UseSystemThemeParams) => {
  const { onThemeChange } = params;

  React.useEffect(() => {
    const listener = (media: MediaQueryList | MediaQueryListEvent) =>
      onThemeChange(getCurrentSystemTheme(media, SystemTheme.Dark));

    const media = matchSystemThemeMedia(SystemTheme.Dark);
    listener(media);

    media.addEventListener('change', listener);
    return () => media.removeEventListener('change', listener);
  }, [onThemeChange]);
};
