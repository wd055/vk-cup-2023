import React from 'react';

import { getContainerNode } from '../helpers';

export const usePopoverContainer = (
  isMounted: boolean,
  containerNode: HTMLElement | undefined | null = null,
) => {
  const [container, setContainer] = React.useState<HTMLElement | null>(null);

  React.useEffect(() => {
    if (!isMounted || container) {
      return;
    }

    setContainer(containerNode || getContainerNode());
  }, [isMounted, container]);

  return { container };
};
