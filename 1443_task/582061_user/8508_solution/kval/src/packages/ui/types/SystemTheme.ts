export enum SystemTheme {
  Light = 'light',
  Dark = 'dark',
}
