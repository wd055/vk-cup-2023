import { SystemTheme } from './SystemTheme';

export type CustomTheme = {
  readonly name: string;
  readonly extendsFrom: SystemTheme;
  readonly overrides: {
    [key: string]: string;
  };
  readonly contrast?: SystemTheme;
  readonly preview?: string;
};
