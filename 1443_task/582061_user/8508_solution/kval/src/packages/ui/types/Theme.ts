import { SystemTheme } from './SystemTheme';
import { CustomTheme } from './CustomTheme';

export type Theme = SystemTheme | CustomTheme;
