import { SystemTheme } from '../types';

export function invertTheme(theme: SystemTheme): SystemTheme {
  switch (theme) {
    case SystemTheme.Light:
      return SystemTheme.Dark;

    case SystemTheme.Dark:
      return SystemTheme.Light;

    default:
      throw new Error(`Unknown theme: ${theme}`);
  }
}
