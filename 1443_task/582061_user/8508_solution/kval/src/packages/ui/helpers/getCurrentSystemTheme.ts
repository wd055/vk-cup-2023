import { invertTheme } from './invertTheme';
import { SystemTheme } from '../types';

export function getCurrentSystemTheme(
  media: MediaQueryList | MediaQueryListEvent,
  matchTheme: SystemTheme,
): SystemTheme {
  if (media.matches) {
    return matchTheme;
  }

  return invertTheme(matchTheme);
}
