import { nanoid } from 'nanoid';

export function getContainerNode(): HTMLDivElement {
  const div = document.createElement('div');
  document.body.appendChild(div);

  div.id = nanoid();
  div.style.position = 'fixed';
  div.style.bottom = '0';
  div.style.width = '100%';
  div.style.height = '0';
  div.style.zIndex = '9999';

  div.ariaHidden = 'true';

  return div;
}
