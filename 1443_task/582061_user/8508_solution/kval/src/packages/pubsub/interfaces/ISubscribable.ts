import { Event, Handler, HandlerParams } from '../types';

export interface ISubscribable<E extends Event, P extends HandlerParams<E>> {
  /**
   * Subscribe to event
   * @param event
   * @param handler
   */
  subscribe<T extends E>(event: T, handler: Handler<T, P>): () => void;

  /**
   * Unsubscribe from event
   * @param event
   * @param handler
   */
  unsubscribe<T extends E>(event: T, handler: Handler<T, P>): void;

  /**
   * Removes all references to event handlers
   */
  dispose(): void;
}
