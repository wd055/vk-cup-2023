export type HandlerParams<T extends string> = {
  [K in T]: any;
};
