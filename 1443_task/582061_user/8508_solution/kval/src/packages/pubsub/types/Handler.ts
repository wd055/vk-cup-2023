import { Event } from './Event';
import { HandlerParams } from './HandlerParams';

export type Handler<E extends Event, P extends HandlerParams<E>> = (
  ...args: P[E]
) => void;
