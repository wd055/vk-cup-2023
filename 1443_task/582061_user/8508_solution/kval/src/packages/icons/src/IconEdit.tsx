import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconEdit = createIcon(
  16,
  16,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="m11.435 6.071-7.36 7.36-1.577.247.195-1.693 7.328-7.328 1.414 1.414Zm1.414-1.414.707-.707a1 1 0 1 0-1.414-1.414l-.707.707 1.414 1.414Zm2.122.707-9.943 9.942-4.816.755.573-4.997 9.943-9.943a3 3 0 0 1 4.243 4.243Z"
    clipRule="evenodd"
  />,
);
