import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconBookmarkOutlined = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M10 11a2 2 0 0 1 1.505.683L13 13.391V5H7v8.391l1.495-1.708A2 2 0 0 1 10 11Zm3.192 5.648a1.032 1.032 0 0 0 1.808-.68V5a2 2 0 0 0-2-2H7a2 2 0 0 0-2 2v10.968a1.032 1.032 0 0 0 1.808.68L10 13l3.192 3.648Z"
    clipRule="evenodd"
  />,
);
