import React from 'react';

import { createIcon } from './icon/createIcon';

export const IconImportant = createIcon(
  21,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M12 15.5a1.5 1.5 0 1 0-3.001.001A1.5 1.5 0 0 0 12 15.5Zm0-11a1.5 1.5 0 0 0-3 0v6a1.5 1.5 0 0 0 3 0v-6Z"
    clipRule="evenodd"
  />,
);
