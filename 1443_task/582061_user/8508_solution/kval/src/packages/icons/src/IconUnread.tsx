import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconUnread = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"
    clipRule="evenodd"
  />,
);
