import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconMoney = createIcon(
  20,
  20,
  <path
    fill="#24C780"
    fillRule="evenodd"
    d="M6.5 3H11a4.5 4.5 0 1 1 0 9H7.5v1h5a1 1 0 1 1 0 2h-5v1a1 1 0 1 1-2 0v-1a1 1 0 1 1 0-2v-1a1 1 0 1 1 0-2V4a1 1 0 0 1 1-1Zm1 7H11a2.5 2.5 0 0 0 0-5H7.5v5Z"
    clipRule="evenodd"
  />,
);
