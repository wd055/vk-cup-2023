import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconCart = createIcon(
  20,
  20,
  <path
    fill="#F94CA3"
    fillRule="evenodd"
    d="M2 4a1 1 0 0 1 1-1h2a1 1 0 0 1 .986.836L6.18 5H17a1 1 0 0 1 1 1v3.85a2.6 2.6 0 0 1-2.16 2.564l-8.875 1.521c-.05.009-.101.013-.15.014a1 1 0 0 1-1.3-.785L4.152 5H3a1 1 0 0 1-1-1Zm5.321 7.845L6.514 7H16v2.85a.6.6 0 0 1-.499.592l-8.18 1.403ZM9 16.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0Zm7 0a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0Z"
    clipRule="evenodd"
  />,
);
