import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconClose = createIcon(
  16,
  16,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M7.983 6.455l4.34-4.34a1.105 1.105 0 011.562 1.562l-4.34 4.34 4.34 4.34a1.08 1.08 0 11-1.528 1.528l-4.34-4.34-4.34 4.34a1.105 1.105 0 01-1.562-1.562l4.34-4.34-4.34-4.34a1.08 1.08 0 111.528-1.528l4.34 4.34z"
  />,
);
