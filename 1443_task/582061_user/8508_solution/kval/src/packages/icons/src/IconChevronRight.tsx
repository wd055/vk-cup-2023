import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconChevronRight = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    clipRule="evenodd"
    d="M7.79289 6.29289c.39053-.39052 1.02369-.39052 1.41422 0l2.99999 3c.3905.39053.3905 1.02371 0 1.41421l-2.99999 3c-.39053.3905-1.02369.3905-1.41422 0-.39052-.3905-.39052-1.0237 0-1.4142L10.0858 10 7.79289 7.70711c-.39052-.39053-.39052-1.02369 0-1.41422z"
  />,
);
