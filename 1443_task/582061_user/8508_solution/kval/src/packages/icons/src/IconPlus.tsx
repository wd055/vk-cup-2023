import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconPlus = createIcon(
  16,
  16,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M8.4 3a.6.6 0 0 1 .6.6V7h3.4a.6.6 0 0 1 .6.6v.8a.6.6 0 0 1-.6.6H9v3.4a.6.6 0 0 1-.6.6h-.8a.6.6 0 0 1-.6-.6V9H3.6a.6.6 0 0 1-.6-.6v-.8a.6.6 0 0 1 .6-.6H7V3.6a.6.6 0 0 1 .6-.6h.8Z"
    clipRule="evenodd"
  />,
);
