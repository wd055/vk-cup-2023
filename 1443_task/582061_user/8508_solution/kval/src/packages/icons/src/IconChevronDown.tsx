import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconChevronDown = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M13.707 7.793a1 1 0 0 1 0 1.414l-3 3a1 1 0 0 1-1.414 0l-3-3a1 1 0 1 1 1.414-1.414L10 10.086l2.293-2.293a1 1 0 0 1 1.414 0Z"
    clipRule="evenodd"
  />,
);
