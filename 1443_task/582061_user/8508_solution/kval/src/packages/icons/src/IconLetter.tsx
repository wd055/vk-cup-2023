import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconLetter = createIcon(
  20,
  20,
  <>
    <path
      fill="currentColor"
      d="M5.293 11.543a1 1 0 1 0 1.414 1.414l1.739-1.738.896.784a1 1 0 0 0 1.316 0l.896-.784 1.739 1.738a1 1 0 0 0 1.414-1.414l-1.644-1.644 1.595-1.396a1 1 0 0 0-1.316-1.506L10 9.921 6.659 6.997a1 1 0 1 0-1.317 1.506l1.595 1.396-1.644 1.644Z"
    />
    <path
      fill="currentColor"
      fillRule="evenodd"
      d="M2 6a3 3 0 0 1 3-3h10a3 3 0 0 1 3 3v8a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V6Zm3-1a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V6a1 1 0 0 0-1-1H5Z"
      clipRule="evenodd"
    />
  </>,
);
