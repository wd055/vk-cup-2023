import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconDownload = createIcon(
  16,
  16,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M5.15 6.241a1 1 0 0 0-1.3 1.519L8 11.316l4.15-3.558a1 1 0 0 0-1.3-1.518L9 7.826V2a1 1 0 0 0-2 0v5.826L5.15 6.241ZM2 13.001a1 1 0 0 0 0 2h12a1 1 0 1 0 0-2H2Z"
    clipRule="evenodd"
  />,
);
