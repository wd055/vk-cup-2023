export * from './src/IconLogo';
export * from './src/IconArchive';
export * from './src/IconAttachment';
export * from './src/IconBookmarkOutlined';
export * from './src/IconBookmarkFilled';
export * from './src/IconDownload';
export * from './src/IconDraft';
export * from './src/IconEdit';
export * from './src/IconLetter';
export * from './src/IconFolder';
export * from './src/IconPalette';
export * from './src/IconPlus';
export * from './src/IconGovernment';
export * from './src/IconKey';
export * from './src/IconMenu';
export * from './src/IconCart';
export * from './src/IconMoney';
export * from './src/IconPlane';
export * from './src/IconSent';
export * from './src/IconSpam';
export * from './src/IconTicket';
export * from './src/IconTrash';
export * from './src/IconLoader';
export * from './src/IconImportant';
export * from './src/IconSettings';
export * from './src/IconCheckmark';
export * from './src/IconClose';
export * from './src/IconChevronDown';
export * from './src/IconChevronRight';
export * from './src/IconUnread';

export * from './src/icon/createIcon';
