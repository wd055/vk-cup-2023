import { getWindowSafe } from './getWindowSafe';

export type NodeInsets = {
  readonly top: number;
  readonly right: number;
  readonly bottom: number;
  readonly left: number;
};

export type GetNodeInsetsReturnType<D> = D extends undefined
  ? NodeInsets
  : NodeInsets | D;

export function getNodeInsets<D = undefined>(
  node?: HTMLElement | null,
  defaultValue?: D,
): GetNodeInsetsReturnType<D> {
  const window = getWindowSafe();

  if (!window || !node) {
    return (defaultValue ||
      ({
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
      } as NodeInsets)) as GetNodeInsetsReturnType<D>;
  }

  const { paddingTop, paddingRight, paddingLeft, paddingBottom } =
    window.getComputedStyle(node);

  const insets = [paddingTop, paddingRight, paddingBottom, paddingLeft].map(
    (value) => {
      const num = parseInt(value, 10);

      if (Number.isNaN(num)) {
        return 0;
      }

      return num;
    },
  );

  return {
    top: insets[0],
    right: insets[1],
    bottom: insets[2],
    left: insets[3],
  } as GetNodeInsetsReturnType<D>;
}
