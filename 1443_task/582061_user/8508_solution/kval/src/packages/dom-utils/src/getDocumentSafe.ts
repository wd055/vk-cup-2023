export function getDocumentSafe(): Document | null {
  if (typeof document === 'undefined') {
    return null;
  }

  return document;
}
