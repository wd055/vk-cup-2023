export function getWindowSafe(): (Window & typeof globalThis) | null {
  if (typeof window === 'undefined') {
    return null;
  }

  return window;
}
