import { ScrollEvent } from './ScrollEvent';

export type ScrollHandlerDefaultParams = [anchor: HTMLElement, event: Event];

export type ScrollHandlerParams = {
  [ScrollEvent.Scroll]: ScrollHandlerDefaultParams;
  [ScrollEvent.End]: ScrollHandlerDefaultParams;
};
