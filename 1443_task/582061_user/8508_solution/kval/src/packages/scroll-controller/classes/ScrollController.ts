import { Subscribable } from 'pubsub';
import { IScrollController } from '../interfaces';
import { ScrollEvent, ScrollHandlerParams } from '../types';

export class ScrollController
  extends Subscribable<ScrollEvent, ScrollHandlerParams>
  implements IScrollController
{
  private anchor: HTMLElement | null;
  private prevScrollTop: number;
  private currentScrollTop: number;

  get scrollSpeed(): number {
    return this.currentScrollTop - this.prevScrollTop;
  }

  get scrollTop(): number {
    return this.currentScrollTop;
  }

  public constructor() {
    super({
      [ScrollEvent.Scroll]: [],
      [ScrollEvent.End]: [],
    });

    this.anchor = null;
    this.prevScrollTop = 0;
    this.currentScrollTop = 0;

    this.handleScroll = this.handleScroll.bind(this);
  }

  public attach(node: HTMLElement): void {
    this.disposeAnchorListeners();

    this.anchor = node;
    this.anchor.addEventListener('scroll', this.handleScroll, {
      passive: true,
    });
  }

  public scrollTo(value: number): void {
    if (!this.anchor) {
      return;
    }

    this.currentScrollTop = value;
    this.anchor.scrollTop = value;
  }

  public dispose(): void {
    super.dispose();
    this.disposeAnchorListeners();

    this.anchor = null;
  }

  /**
   * Handles node scroll event
   * @param event scroll event
   */
  private handleScroll(event: Event): void {
    if (!this.anchor) {
      return;
    }

    this.prevScrollTop = this.currentScrollTop;
    this.currentScrollTop = this.anchor.scrollTop;

    this.publish(ScrollEvent.Scroll, this.anchor, event);

    if (
      this.currentScrollTop ===
      this.anchor.scrollHeight - this.anchor.clientHeight
    ) {
      this.publish(ScrollEvent.End, this.anchor, event);
    }
  }

  /**
   * Remove events listeners attached to the anchor
   */
  private disposeAnchorListeners(): void {
    if (!this.anchor) {
      return;
    }

    this.anchor.removeEventListener('scroll', this.handleScroll);
  }
}
