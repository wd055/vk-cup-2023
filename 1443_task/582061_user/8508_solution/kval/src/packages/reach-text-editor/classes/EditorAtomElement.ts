/* eslint-disable class-methods-use-this */
import { IEditorAtomElement } from '../interfaces';
import { ElementAttributes, ElementType } from '../types';
import { EditorElement } from './Element';

export class EditorAtomElement
  extends EditorElement<ElementType.Atom>
  implements IEditorAtomElement
{
  public constructor(
    public children: (string | IEditorAtomElement)[],
    attributes: ElementAttributes,
  ) {
    super(ElementType.Atom, attributes);
  }

  public canMerge(): boolean {
    return true;
  }

  public merge(): boolean {
    return true;
  }
}
