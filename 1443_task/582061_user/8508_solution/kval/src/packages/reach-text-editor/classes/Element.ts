import { Subscribable } from 'pubsub';

import { IEditorElement } from '../interfaces';
import {
  ElementAttributes,
  ElementEvent,
  ElementEventHandlerParams,
  ElementType,
} from '../types';

export class EditorElement<T extends ElementType = ElementType>
  extends Subscribable<ElementEvent, ElementEventHandlerParams>
  implements IEditorElement<T>
{
  public constructor(
    public readonly type: T,
    public readonly attributes: ElementAttributes,
  ) {
    super({
      [ElementEvent.Focus]: [],
      [ElementEvent.Render]: [],
      [ElementEvent.Merge]: [],
    });
  }

  public focus(): void {
    setTimeout(() => {
      this.publish(ElementEvent.Focus);
    }, 1);
  }

  // eslint-disable-next-line class-methods-use-this
  public canMerge(): boolean {
    return true;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars, class-methods-use-this
  public merge(element: IEditorElement<ElementType>): boolean {
    return false;
  }

  public render(): void {
    this.publish(ElementEvent.Render);
  }
}
