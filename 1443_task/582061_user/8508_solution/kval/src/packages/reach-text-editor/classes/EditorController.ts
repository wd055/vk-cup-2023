import { Subscribable } from 'pubsub';

import { Node, EditorEvent, EditorEventHandlerParams } from '../types';
import {
  IEditorAtomElement,
  IEditorController,
  IEditorElement,
} from '../interfaces';

import { EditorAtomElement } from './EditorAtomElement';

let counter = 0;

function generateId(): string {
  const id = `${counter}`;

  counter += 1;

  return id;
}

export class EditorController
  extends Subscribable<EditorEvent, EditorEventHandlerParams>
  implements IEditorController
{
  private nodes: Node[];
  private nodeIndex: { [key: string]: number };

  public constructor() {
    super({
      [EditorEvent.Render]: [],
    });

    this.nodes = [];
    this.nodeIndex = {};
  }

  public insertElement(element: IEditorElement, position?: number): Node {
    const hasPosition = typeof position === 'number' && !Number.isNaN(position);
    const index = hasPosition
      ? Math.min(position, this.nodes.length)
      : this.nodes.length;

    const node: Node = {
      element,
      id: generateId(),
    };

    this.nodes = [
      ...this.nodes.slice(0, index),
      node,
      ...this.nodes.slice(index + 1),
    ];

    this.updateIndexes(index);

    return node;
  }

  public getNodes(): Node[] {
    return this.nodes;
  }

  public handleAtomInputBreak(node: Node<IEditorAtomElement>): void {
    // const index = this.nodeIndex[node.id];
    // if (typeof index !== 'number') {
    //   return;
    // }
    // const element = new EditorAtomElement('', {});
    // this.insertElement(element, index + 1);
    // element.focus();
  }

  public handleAtomBackspace(
    node: Node<IEditorAtomElement>,
    caretPosition: number,
  ): void {}

  private tryRemoveNode(node: Node): [isRemoved: boolean, index: number] {
    if (this.nodes.length === 1) {
      return [false, -1];
    }

    const index = this.nodeIndex[node.id];

    if (!index) {
      return [false, -1];
    }

    node.element.dispose();
    this.nodes.splice(index, 1);
    this.updateIndexes();

    return [true, index];
  }

  private updateIndexes(fromIndex?: number): void {
    for (let i = fromIndex || 0; i < this.nodes.length; i += 1) {
      const node = this.nodes[i];

      this.nodeIndex[node.id] = i;
    }

    this.publish(EditorEvent.Render);
  }
}
