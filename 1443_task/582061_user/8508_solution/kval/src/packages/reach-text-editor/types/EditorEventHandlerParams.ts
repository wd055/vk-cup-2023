import { EditorEvent } from './EditorEvent';

export type EditorEventHandlerParams = {
  [EditorEvent.Render]: [];
};
