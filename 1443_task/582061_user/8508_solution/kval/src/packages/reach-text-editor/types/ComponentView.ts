import { IEditorController } from 'src/packages/reach-text-editor/interfaces';
import { Node, Element, AtomElement, ComponentElement } from './Node';

export type ElementViewProps<T extends Element> = {
  readonly node: Node<T>;
  readonly controller: IEditorController;
};

export type ElementView = React.FC<ElementViewProps<Element>>;

export type ComponentView = React.FC<ElementViewProps<ComponentElement>>;
export type AtomView = React.FC<ElementViewProps<AtomElement>>;
