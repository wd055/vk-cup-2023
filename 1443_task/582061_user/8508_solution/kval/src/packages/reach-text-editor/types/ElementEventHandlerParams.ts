import { ElementEvent } from './ElementEvent';

export type ElementEventHandlerParams = {
  [ElementEvent.Focus]: [];
  [ElementEvent.Render]: [];
  [ElementEvent.Merge]: [];
};
