import React from 'react';

import { ElementEvent, Node } from '../../types';
import { IEditorAtomElement, IEditorController } from '../../interfaces';
import { useInputHandler, useKeydownHandler } from '../../hooks';

import styles from './EditorAtomView.module.scss';

export type EditorAtomViewProps = {
  readonly node: Node<IEditorAtomElement>;
  readonly controller: IEditorController;
};

export const EditorAtomView: React.FC<EditorAtomViewProps> = ({
  node,
  controller,
}) => {
  const { element } = node;
  const { attributes, children } = element;
  const { color, background } = attributes;

  const spanRef = React.useRef<HTMLSpanElement>(null);

  const { handleInput } = useInputHandler({
    node,
    controller,
  });
  const { handleKeydown } = useKeydownHandler({ node, controller });

  React.useEffect(() => element.subscribe(ElementEvent.Focus, () => {}), []);

  React.useEffect(() => element.subscribe(ElementEvent.Merge, () => {}), []);

  return (
    <span
      ref={spanRef}
      className={styles.root}
      style={{ color, background }}
      onInput={(event) => {
        console.log(event.currentTarget.innerHTML);
      }}
      onKeyDownCapture={(event) => {
        if (event.code === 'KeyB' && (event.ctrlKey || event.metaKey)) {
          event.preventDefault();
          const selection = window.getSelection();

          if (!selection) {
            return;
          }

          const currentSelectionRange = selection.getRangeAt(0);

          console.log(currentSelectionRange.extractContents());
        }
      }}
      contentEditable="true"
    >
      {children.map((Child) => {
        if (typeof Child === 'string') {
          return <>{Child}</>;
        }

        return null;
      })}
    </span>
  );
};
