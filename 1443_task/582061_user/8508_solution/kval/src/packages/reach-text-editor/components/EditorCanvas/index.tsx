import React from 'react';

import { IEditorController, IEditorRenderer } from '../../interfaces';
import { Node, EditorEvent } from '../../types';
import { DEFAULT_RENDERER } from '../../constants';

export type EditorCanvasProps = {
  readonly className?: string;
  readonly controller: IEditorController;
  readonly renderer?: IEditorRenderer;
};

export const EditorCanvas: React.FC<EditorCanvasProps> = ({
  className,
  controller,
  renderer = DEFAULT_RENDERER,
}) => {
  const [nodes, setNodes] = React.useState<Node[]>(controller.getNodes());

  React.useEffect(
    () =>
      controller.subscribe(EditorEvent.Render, () =>
        setNodes([...controller.getNodes()]),
      ),
    [],
  );

  return (
    <div
      className={className}
      // contentEditable="true"
    >
      {nodes.map((node) => {
        const View = renderer.getElementView(node.element);

        if (!View) {
          return null;
        }

        return <View key={node.id} node={node} controller={controller} />;
      })}
    </div>
  );
};
