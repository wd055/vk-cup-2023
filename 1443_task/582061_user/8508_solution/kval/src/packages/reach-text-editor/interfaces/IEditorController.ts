import { ISubscribable } from 'pubsub';

import { Node, EditorEvent, EditorEventHandlerParams } from '../types';
import { IEditorAtomElement, IEditorElement } from './IElement';

export interface IEditorController
  extends ISubscribable<EditorEvent, EditorEventHandlerParams> {
  insertElement(element: IEditorElement): Node;

  getNodes(): Node[];

  handleAtomInputBreak(node: Node<IEditorAtomElement>): void;

  handleAtomBackspace(
    node: Node<IEditorAtomElement>,
    caretPosition: number,
  ): void;
}
