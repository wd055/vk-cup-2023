import React from 'react';

import { IEditorAtomElement, IEditorController } from '../interfaces';
import { Node } from '../types';

export type UseKeydownHandler = {
  readonly node: Node<IEditorAtomElement>;
  readonly controller: IEditorController;
};

export const useKeydownHandler = (params: UseKeydownHandler) => {
  const { node, controller } = params;

  const handleKeydown = React.useCallback(
    (event: React.KeyboardEvent<HTMLSpanElement>) => {
      event.preventDefault();

      return;

      // TODO: move key detection to separate library
      switch (event.key) {
        case 'Enter':
          event.preventDefault();
          controller.handleAtomInputBreak(node);
          break;

        case 'Backspace':
          {
            const selection = window.getSelection();

            if (selection) {
              controller.handleAtomBackspace(node, selection.focusOffset);
            } else {
              console.warn(
                '[REACH_TEXT_EDITOR]: THERE IS NO SELECTION AVAILABLE!',
              );
            }
          }
          break;

        default:
          break;
      }
    },
    [node, controller],
  );

  return {
    handleKeydown,
  };
};
