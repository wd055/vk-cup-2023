import React from 'react';

import { IEditorController } from '../interfaces';
import { Node, AtomElement } from '../types';

export type UseInputHandlerParams = {
  readonly node: Node<AtomElement>;
  readonly controller: IEditorController;
};

export const useInputHandler = (params: UseInputHandlerParams) => {
  const { node, controller } = params;

  const handleInput = React.useCallback(
    (event: React.FormEvent<HTMLSpanElement>) => {},
    [node, controller],
  );

  return {
    handleInput,
  };
};
