export type TriggerAnchorCallback = (anchor: HTMLElement) => void;
export type TriggerStateChange = (isOpened: boolean) => void;

export type EventHandler<T> = (event: T) => void;

export enum TriggerEvent {
  onClick = 'onClick',
  onMouseEnter = 'onMouseEnter',
  onMouseLeave = 'onMouseLeave',
}

export type TriggerEventHandlers = {
  [K in TriggerEvent]?: React.EventHandler<any>;
};

export type TriggerHandlersFactory<O extends object> = (
  options: O,
  setAnchor: React.Dispatch<React.SetStateAction<HTMLElement | null>>,
  setIsOpened: React.Dispatch<React.SetStateAction<boolean>>,
) => {
  readonly props: TriggerEventHandlers;
  readonly dispose: () => void;
};

export type Trigger<K extends string, O extends object> = {
  readonly key: K;
  readonly defaultOptions: O;
  readonly createHandlers: TriggerHandlersFactory<O>;
};
