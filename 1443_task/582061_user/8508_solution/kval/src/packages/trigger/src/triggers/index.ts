import { TriggerEvent } from '../types';
import { createTargetHandler, createTrigger, createScheduler } from '../utils';

export const HOVER_TRIGGER = createTrigger(
  'hover',
  {
    mouseEnterDelay: 0.5,
  },
  (options, setAnchor, setIsOpened) => {
    const { mouseEnterDelay } = options;

    const scheduler = createScheduler();

    return {
      props: {
        [TriggerEvent.onMouseEnter]: createTargetHandler((target) => {
          setAnchor(target);
          scheduler.delay(mouseEnterDelay * 1000, () => setIsOpened(true));
        }),

        [TriggerEvent.onMouseLeave]: createTargetHandler(() => {
          scheduler.clear();
          setAnchor(null);
          setIsOpened(false);
        }),
      },

      dispose: () => scheduler.clear(),
    };
  },
);

export const CLICK_TRIGGER = createTrigger(
  'click',
  {},
  (options, setAnchor, setIsOpened) => ({
    props: {
      [TriggerEvent.onClick]: createTargetHandler((target, event) => {
        event.preventDefault();

        setAnchor(target);
        setIsOpened((isOpened) => !isOpened);
      }),
    },

    dispose: () => {},
  }),
);

export const TRIGGERS = {
  hover: HOVER_TRIGGER,
  click: CLICK_TRIGGER,
};

export type TriggerKey = keyof typeof TRIGGERS;
