import { Subscribable } from 'pubsub';

import {
  Language,
  I18nResource,
  I18nEvent,
  I18nEventHandlerParams,
} from './types';

export class I18n extends Subscribable<I18nEvent, I18nEventHandlerParams> {
  private static instance: I18n;

  private lang: Language = Language.Ru;
  private resource: I18nResource = {};

  public static get Instance(): I18n {
    return I18n.instance;
  }

  public constructor() {
    super({
      [I18nEvent.Init]: [],
      [I18nEvent.LanguageChange]: [],
    });

    this.load = this.load.bind(this);
    this.translate = this.translate.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
    this.getLanguage = this.getLanguage.bind(this);
    this.getLanguageName = this.getLanguageName.bind(this);

    if (I18n.instance) {
      I18n.instance.dispose();
    }

    I18n.instance = this;
  }

  public static init(): void {
    I18n.instance = new I18n();
  }

  public load(lang: Language, resource: I18nResource): void {
    this.lang = lang;
    this.resource = resource;

    this.publish(I18nEvent.Init, this.lang);
  }

  // TODO: make case insensitive
  public translate(key: string): string {
    const path = key.split('.');

    let value = this.resource;
    for (const segment of path) {
      if (!value) {
        return key;
      }

      value = value[segment];
    }

    if (typeof value !== 'string') {
      return key;
    }

    return value;
  }

  public getLanguage(): Language {
    return this.lang;
  }

  public changeLanguage(language: Language): void {
    const prevLanguage = this.lang;
    this.lang = language;

    this.publish(I18nEvent.LanguageChange, this.lang, prevLanguage);
  }

  // eslint-disable-next-line class-methods-use-this
  public getLanguageName(language: Language): string {
    switch (language) {
      case Language.Ru:
        return 'Русский';

      case Language.En:
        return 'English';

      default:
        return language;
    }
  }
}
