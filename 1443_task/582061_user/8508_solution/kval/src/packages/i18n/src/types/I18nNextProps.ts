import { Language } from './Language';
import { I18nResource } from './I18nResource';

export type I18nNextProps = {
  readonly lang: Language;
  readonly i18nResource: I18nResource;
};
