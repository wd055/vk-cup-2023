import { GetStaticProps, GetStaticPropsContext } from 'next';

import { I18nNextProps, I18nResource, Language } from '../types';
import { parseLanguage } from './parseLanguage';

export function createGetStaticProps(
  loadResource: (lang: Language) => I18nResource,
): GetStaticProps<I18nNextProps> {
  return (context: GetStaticPropsContext) => {
    const language = parseLanguage(context.params?.lang?.[0], Language.Ru);
    const resource = loadResource(language);

    return {
      props: {
        lang: language,
        i18nResource: resource,
      },
      revalidate: false,
    };
  };
}
