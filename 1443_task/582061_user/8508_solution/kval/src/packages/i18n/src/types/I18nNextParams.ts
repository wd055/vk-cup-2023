import { Language } from './Language';

export type I18nNextParams = { readonly lang: Language[] };
