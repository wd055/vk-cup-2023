import { Language } from '../types';

export function parseLanguage(
  lang: string | string[] | null | undefined,
  fallback: Language,
): Language {
  const language = Object.values(Language).includes(lang as Language)
    ? (lang as Language)
    : fallback;

  return language;
}
