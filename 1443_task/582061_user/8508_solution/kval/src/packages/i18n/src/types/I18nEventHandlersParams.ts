import { Language } from './Language';
import { I18nEvent } from './I18nEvent';

export type I18nEventHandlerParams = {
  [I18nEvent.Init]: [language: Language];
  [I18nEvent.LanguageChange]: [language: Language, prevLanguage: Language];
};
