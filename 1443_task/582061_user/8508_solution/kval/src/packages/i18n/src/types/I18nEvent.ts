export enum I18nEvent {
  Init = 'init',
  LanguageChange = 'languagechange',
}
