import { createServer } from 'node:http';
import { Router } from './classes/Router';
import { RequestMethod } from './types';

export class Server {
  private routers: Router[];

  public constructor() {
    this.routers = [];
  }

  public addRouter(router: Router): void {
    this.routers.push(router);
  }

  public listen(port: number, callback: () => void): void {
    const server = createServer(async (req, res) => {
      for (let i = 0; i < this.routers.length; i += 1) {
        const router = this.routers[i];
        await router.handle(
          req.url || '/',
          req.method as RequestMethod,
          req,
          res,
        );
      }
    });

    server.listen(port, callback);
  }
}
