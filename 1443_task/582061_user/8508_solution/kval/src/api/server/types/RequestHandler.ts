import { ServerRequest } from './ServerRequest';
import { ServerResponse } from './ServerResponse';

export type RequestHandler = (
  req: ServerRequest,
  res: ServerResponse,
) => Promise<void>;
