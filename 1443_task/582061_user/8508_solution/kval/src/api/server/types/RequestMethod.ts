export enum RequestMethod {
  GET = 'GET',
}

export const REQUEST_METHODS = [RequestMethod.GET];
