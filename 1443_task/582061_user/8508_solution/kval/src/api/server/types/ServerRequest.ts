import { IncomingMessage } from 'node:http';

export type ServerRequest = IncomingMessage & {
  params: Record<string, string>;
  query: URLSearchParams;
};
