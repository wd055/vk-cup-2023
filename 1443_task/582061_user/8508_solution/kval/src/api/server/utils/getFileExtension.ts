export function getFileExtension(filePath: string): string {
  const ext = filePath.match(/\.\w{2,4}($|\?)/im)?.[0];

  if (!ext) {
    throw new Error(`Could not extract file extension. file: ${filePath}`);
  }

  return ext.replace(/\./g, '');
}
