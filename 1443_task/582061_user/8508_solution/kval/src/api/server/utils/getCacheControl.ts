import { Mimetype } from '../../constants';

export function getCacheControl(mimetype: Mimetype): string {
  switch (mimetype) {
    case Mimetype.Html:
    case Mimetype.Js:
    case Mimetype.Css:
    case Mimetype.Json:
      return 'no-cache, no-store';

    case Mimetype.Jpg:
    case Mimetype.Png:
    case Mimetype.Svg:
      return 'public, max-age=15552000';

    default:
      return 'no-cache, no-store';
  }
}
