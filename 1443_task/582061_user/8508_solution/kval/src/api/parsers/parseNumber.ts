export function parseNumber(str: string, defaultValue = NaN): number {
  const value = Number(str);

  if (Number.isNaN(value)) {
    return defaultValue;
  }

  return value;
}
