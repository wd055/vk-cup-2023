import { Registry } from './Registry';
import { Server } from './server';
import { router } from './routes';
import { LetterRepository } from './repositories';

const PORT = 3000;
const DB_PATH = './db.json';

const server = new Server();

server.addRouter(router);

export async function run() {
  // Connect to DB
  console.log(`Connecting to DB on path: ${DB_PATH}...`);

  const db = Registry.getDb();

  const indexes = LetterRepository.getIndexes();

  for (const index of indexes) {
    db.addIndex(index);
  }

  await db.connect(DB_PATH);

  console.log('DB connected successfully.');

  // Start server
  server.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}...`);
  });
}

run();
