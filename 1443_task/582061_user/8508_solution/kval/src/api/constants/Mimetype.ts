export enum Mimetype {
  Html = 'text/html',
  Css = 'text/css',
  Js = 'text/javascript',
  Png = 'image/png',
  Jpg = 'image/jpg',
  Json = 'application/json',
  Svg = 'image/svg+xml',
}

export const MimetypeExtensionMap = {
  [Mimetype.Html]: 'html',
  [Mimetype.Css]: 'css',
  [Mimetype.Js]: 'js',
  [Mimetype.Png]: 'png',
  [Mimetype.Jpg]: 'jpg',
  [Mimetype.Json]: 'json',
  [Mimetype.Svg]: 'svg',
};

export const MimetypeExtensionReverseMap = Object.keys(
  MimetypeExtensionMap,
).reduce(
  (acc, key) => ({
    ...acc,
    [MimetypeExtensionMap[key as Mimetype]]: key as Mimetype,
  }),
  {
    jpeg: Mimetype.Jpg,
  } as { [key: string]: Mimetype },
);
