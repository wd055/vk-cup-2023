export enum IndexType {
  Key = 'KEY',
  Sort = 'SORT',
  Map = 'MAP',
}
