export type QueryWhereClause = {
  readonly field: string;
  readonly value: any;
};

export type QuerySortClause = {
  readonly field: string;
  readonly order: number;
};

export type PlainQuery = {
  select: string[];
  where: QueryWhereClause[];
  sort: QuerySortClause[];
  skip: number | undefined;
  limit: number | undefined;
  mapFields: string[];
};
