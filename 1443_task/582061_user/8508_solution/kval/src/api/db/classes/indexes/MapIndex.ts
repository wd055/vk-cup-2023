import { IndexType, RecordPointer } from '../../types';
import { IMapIndex } from '../../interfaces';

export type MapIndexOptions = {
  readonly field: string;
  readonly toField: string;

  readonly map: (value: any) => any;
};

export class MapIndex implements IMapIndex {
  public readonly type: IndexType.Map;
  public readonly field: string;

  protected readonly map: Map<number, any>;

  public constructor(public readonly options: MapIndexOptions) {
    this.type = IndexType.Map;
    this.field = options.field;

    this.map = new Map();
  }

  public add(pointer: RecordPointer, record: any): void {
    this.map.set(pointer.id, this.options.map(record[this.field]));
  }

  public get(id: number): [key: string, value: any] {
    const value = this.map.get(id);

    return [this.options.toField, value];
  }
}
