import { isNumberChar } from './utils';
import { Reader } from './types';

export function createJsonNumberReader(
  onValue: (position: number, length: number) => void,
): Reader {
  let valueStartByteIndex = 0;
  let isValueReadStarted = false;

  return {
    next: (char, index, chunkSize, chunksReadCount) => {
      if (!isValueReadStarted && isNumberChar(char)) {
        isValueReadStarted = true;
        valueStartByteIndex = chunksReadCount * chunkSize + index;
      }

      if (isValueReadStarted && !isNumberChar(char)) {
        onValue(
          valueStartByteIndex,
          chunksReadCount * chunkSize + index - valueStartByteIndex,
        );
      }
    },
  };
}
