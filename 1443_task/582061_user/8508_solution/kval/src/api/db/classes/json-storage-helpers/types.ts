export type Reader = {
  next(
    char: string | number,
    index: number,
    chunkSize: number,
    chunksReadCount: number,
    chunk: string | Buffer,
  ): void;
};
