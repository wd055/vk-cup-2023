import { QueryWhereClause, RecordPointer } from '../types';
import { IKeyIndex } from '../interfaces';

type IndexWithValue = {
  readonly index: IKeyIndex;
  readonly value: string;
};

export class CompoundKeyIndex {
  public constructor(
    private readonly mainIndex: IndexWithValue | null,
    private readonly subindexes: IndexWithValue[] = [],
  ) {}

  /**
   * Filters pointers from compound index
   * @param pointers
   * @returns
   */
  public filter(pointers: readonly RecordPointer[]): readonly RecordPointer[] {
    if (!this.mainIndex) {
      return pointers;
    }

    const { index, value } = this.mainIndex;
    const pointersToFilter = index.getAll(value);

    const result: RecordPointer[] = [];

    for (let i = 0; i < pointersToFilter.length; i += 1) {
      const pointer = pointersToFilter[i];
      const isMatchesSubindexes = this.subindexes.every(({ index, value }) =>
        index.has(value, pointer.id),
      );

      if (isMatchesSubindexes) {
        result.push(pointer);
      }
    }

    return result;
  }

  /**
   * Creates compound index from clauses and available indexes
   * @param clauses query `where` clauses
   * @param indexMap available indexes to use
   */
  public static fromClauses(
    clauses: QueryWhereClause[],
    indexMap: Map<string, IKeyIndex>,
  ): CompoundKeyIndex {
    const subindexes: IndexWithValue[] = [];
    let minIndex: IndexWithValue | null = null;

    for (let i = 0; i < clauses.length; i += 1) {
      const clause = clauses[i];
      const index = indexMap.get(clause.field) as IKeyIndex;

      if (!index) {
        continue;
      }

      const candidate: IndexWithValue = {
        index,
        value: clause.value,
      };

      if (!minIndex) {
        minIndex = candidate;
        continue;
      }

      if (
        minIndex.index.getCount(minIndex.value) >
        candidate.index.getCount(candidate.value)
      ) {
        subindexes.push(minIndex);
        minIndex = candidate;

        continue;
      }

      subindexes.push(candidate);
    }

    return new CompoundKeyIndex(minIndex, subindexes);
  }
}
