import { isBooleanChar } from './utils';
import { Reader } from './types';

export function createJsonBooleanReader(
  onValue: (position: number, length: number) => void,
): Reader {
  let valueStartByteIndex = 0;
  let isValueReadStarted = false;

  return {
    next: (char, index, chunkSize, chunksReadCount) => {
      if (!isValueReadStarted && isBooleanChar(char)) {
        isValueReadStarted = true;
        valueStartByteIndex = chunksReadCount * chunkSize + index;
      }

      if (isValueReadStarted && !isBooleanChar(char)) {
        onValue(
          valueStartByteIndex,
          chunksReadCount * chunkSize + index - valueStartByteIndex,
        );
      }
    },
  };
}
