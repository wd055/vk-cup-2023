import { IndexType, RecordPointer } from '../../types';
import { ISortIndex } from '../../interfaces';

export type SortIndexOptions = {
  readonly field: string;

  readonly prepareValue?: (record: any) => any;
  readonly sort?: (valueA: any, valueB: any) => number;
};

type Options = Required<SortIndexOptions>;

export class SortIndex implements ISortIndex {
  public readonly type: IndexType.Sort;
  public readonly field: string;
  public readonly options: Options;

  protected readonly map: Map<number, any>;

  public constructor(options: SortIndexOptions) {
    this.type = IndexType.Sort;
    this.field = options.field;
    this.options = {
      prepareValue: (record) => record[options.field],
      sort: (a, b) => a - b,
      ...options,
    };

    this.map = new Map();
  }

  public add(pointer: RecordPointer, record: any): void {
    const { id } = pointer;
    const value = this.options.prepareValue(record);

    this.map.set(id, value);
  }

  public getScore(idA: number, idB: number, order: number): number {
    const valueA = this.map.get(idA);
    const valueB = this.map.get(idB);

    const score = this.options.sort(valueA, valueB) * Math.sign(order);

    return score;
  }
}
