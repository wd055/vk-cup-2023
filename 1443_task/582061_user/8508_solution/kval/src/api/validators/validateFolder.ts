import { Folder } from 'types';

export function validateFolder(folder: Folder) {
  if (!Object.values(Folder).includes(folder)) {
    throw new Error('Folder incorrect.');
  }

  return folder;
}
