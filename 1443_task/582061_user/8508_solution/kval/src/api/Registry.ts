import { Db } from './db';
import { LetterRepository } from './repositories/LetterRepository';

export abstract class Registry {
  private static db: Db;

  private static letterRepository: LetterRepository;

  public static getDb(): Db {
    if (!this.db) {
      this.db = new Db();
    }

    return this.db;
  }

  public static getLetterRepository(): LetterRepository {
    if (!this.letterRepository) {
      this.letterRepository = new LetterRepository(Registry.getDb());
    }

    return this.letterRepository;
  }
}
