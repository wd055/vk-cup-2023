import { Mimetype, MimetypeExtensionMap } from '../constants';

export function getExtension(mimetype: Mimetype): string {
  const extension = MimetypeExtensionMap[mimetype];

  if (!extension) {
    throw new Error(`Unknown mimetype: ${mimetype}. Could not get extension.`);
  }

  return extension;
}
