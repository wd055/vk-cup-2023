import { FolderTitleReverseMap, Letter, LetterListItem } from 'types';
import { Db, IIndex, KeyIndex, MapIndex, Query, SortIndex } from '../db';

export class LetterRepository {
  public constructor(private readonly db: Db) {}

  public static getIndexes(): IIndex[] {
    return [
      new KeyIndex({
        field: 'folder',
        mapFieldToKey: (value) => {
          if (!value) {
            return 'inbox';
          }

          return FolderTitleReverseMap[value] || null;
        },
      }),
      new KeyIndex({
        field: 'read',
        mapFieldToKey: (value) => !value,
      }),
      new KeyIndex({
        field: 'bookmark',
        mapFieldToKey: (value) => Boolean(value),
      }),
      new KeyIndex({
        field: 'doc',
        mapFieldToKey: (value) => Boolean(value),
      }),
      new SortIndex({
        field: 'date',
        sort: (a, b) => (a < b ? -1 : 1),
      }),
      new SortIndex({
        field: 'author',
        sort: (a: string, b: string) =>
          a.toLowerCase().localeCompare(b.toLowerCase()),
        prepareValue: (record) =>
          `${record.author.name}${record.author.surname}`,
      }),
      new SortIndex({
        field: 'title',
        sort: (a: string, b: string) =>
          a.toLowerCase().localeCompare(b.toLowerCase()),
      }),
      new MapIndex({
        field: 'doc',
        toField: 'attachment',
        map: (doc) => Boolean(doc),
      }),
    ];
  }

  public async getById(id: number): Promise<Letter> {
    return this.db.getById(id);
  }

  public async getMany(
    folder: string,
    page: number,
    limit: number,
    filters: {
      unread: boolean;
      bookmark: boolean;
      doc: boolean;
    },
    sort: string = 'newest',
  ): Promise<[LetterListItem[], number]> {
    const query = new Query()
      .select([
        'author',
        'title',
        'text',
        'bookmark',
        'important',
        'read',
        'folder',
        'date',
        'flag',
      ])
      .where('folder', folder)
      .map('doc')
      .skip(page * limit)
      .limit(limit);

    Object.entries(filters).forEach(([key, value]) => {
      if (value) {
        query.where(key, value);
      }
    });

    switch (sort) {
      case 'newest':
      case 'oldest':
        query.sortBy('date', sort === 'newest' ? -1 : 1);
        break;

      case 'sender-az':
      case 'sender-za':
        query.sortBy('author', sort === 'sender-az' ? 1 : -1);
        break;

      case 'subject-az':
      case 'subject-za':
        query.sortBy('title', sort === 'subject-az' ? 1 : -1);
        break;

      default:
        break;
    }

    return this.db.getMany<LetterListItem>(query.build());
  }

  public async getDoc(id: number): Promise<Letter['doc']> {
    const { doc } = await this.db.getById<any>(id, ['doc']);

    return doc;
  }
}
