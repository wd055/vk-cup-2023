export const Done = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="none">
    <path
      fill="currentColor"
      fillRule="evenodd"
      d="M31 14c-1-1-2.5-1-3.5 0l-10 10-5-5c-1-1-2.5-1-3.5 0s-1 2.5 0 3.5l6.8 6.8a2.4 2.4 0 0 0 3.5-.1L31 17.6c1-1 1-2.5 0-3.5Z"
      clipRule="evenodd"
    />
  </svg>
);
