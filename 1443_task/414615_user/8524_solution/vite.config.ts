import { defineConfig } from 'vite'
import solidPlugin from 'vite-plugin-solid'
import checker from 'vite-plugin-checker'
import solidSvg from 'vite-plugin-solid-svg'
import { visualizer } from "rollup-plugin-visualizer";

export default defineConfig({
  plugins: [
    solidPlugin(),
    checker({ typescript: true }),
    solidSvg({ defaultAsComponent: false }),
    visualizer()
  ],
  server: {
    port: 3008,
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
      },
      '/file': {
        target: 'http://localhost:3000',
      },
    },
  },
  root: 'src',
  build: {
    target: 'esnext',
    outDir: '../dist',
  },
})
