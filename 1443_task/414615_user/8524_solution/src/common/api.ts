export const ApiFolders = [
  'Входящие',
  'Важное',
  'Отправленные',
  'Черновики',
  'Архив',
  'Спам',
  'Корзина',
]

export const ApiFlags = [
  'Заказы',
  'Финансы',
  'Регистрации',
  'Путешествия',
  'Билеты',
  'Штрафы и налоги',
]

export type ApiUser = {
  n: string /* name */
  s: string /* surname */
  e: string /* email */
  a: number /* avatar */
}

export type ApiMsg = {
  id: number

  a: ApiUser /* author */
  to: ApiUser[] /* to */
  t: string /* title */
  te: string /* text */
  st: string /* short text */
  b: boolean /* bookmark */
  i: boolean /* important */
  r: boolean /* read */

  f: number /* folder */
  d: number /* date */
  do: number /* doc */
  ds: number /* doc size */
  fl: number /* flag */
}

export type ApiMsgTitle = {
  id: number

  a: ApiUser /* author */
  t: string /* title */
  st: string /* short text */
  b: boolean /* bookmark */
  i: boolean /* important */
  r: boolean /* read */
  f: number /* folder */

  do: number /* doc */
  d: number /* date */
  fl: number /* flag */
}
