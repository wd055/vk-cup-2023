import {
  Component,
  ComponentProps,
  JSX,
  mergeProps,
  splitProps,
  Accessor,
} from 'solid-js'

import Button from '../common/Button'
import styles from './Toolbar.module.css'

interface ButtonProps extends JSX.ButtonHTMLAttributes<HTMLButtonElement> {
  icon?: Component<ComponentProps<'svg'>>
  active?: boolean
}

const ToolbarButton: Component<ButtonProps> = (props) => {
  const propsMerged = mergeProps({ active: false }, props)
  let [p, a] = splitProps(propsMerged, ['classList', 'icon', 'children'])

  return (
    <Button
      {...a}
      classList={{
        ...p.classList,
        [styles.btn]: true,
        [styles.btnActive]: a.active,
      }}
    >
      {p.icon && <p.icon />}
      <span class={styles.expandedText}>{p.children}</span>
    </Button>
  )
}

export default ToolbarButton
