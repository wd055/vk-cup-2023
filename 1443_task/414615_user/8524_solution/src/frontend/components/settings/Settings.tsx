import {
  Component,
  createSignal,
  For,
  Match,
  ParentComponent,
  Show,
  Switch,
} from 'solid-js'
import { useGlobal } from '../../GlobalContext'
import Button from '../common/Button'
import Popup from '../common/Popup'
import styles from './Settings.module.css'
import RuLangIcon from './assets/ru.svg?component-solid'
import EnLangIcon from './assets/en.svg?component-solid'
import TickIcon from './assets/tick.svg?component-solid'
import { LanguageId } from '../../lang'

import themeDarkIcon from './assets/theme_dark.svg'
import themeLightIcon from './assets/theme_light.svg'
import themeAnimeIcon from './assets/theme_anime.png'
import { ColorThemes } from '../../theme'

const PageSelectButton: ParentComponent<{
  active: boolean
  onClick: () => void
}> = (p) => {
  return (
    <Button
      active={p.active}
      onClick={p.onClick}
      classList={{
        [styles.pageSelect]: true,
        [styles.activePage]: p.active,
      }}
    >
      {p.children}
    </Button>
  )
}

const Radio: ParentComponent<{
  selected: boolean
  onClick: () => void
}> = (p) => {
  return (
    <div class={styles.langSelect} onClick={p.onClick}>
      <div
        classList={{
          [styles.radioBtn]: true,
          [styles.radioActive]: p.selected,
        }}
      >
        <div class={styles.radioBullet} />
      </div>
      {p.children}
    </div>
  )
}

const ThemeSelectButton: ParentComponent<{
  active: boolean
  setActive: () => void
}> = (p) => {
  return (
    <div class={styles.themeSelect} onClick={() => p.setActive()}>
      {p.children}
      <Show when={p.active}>
        <div class={styles.themeActive}>
          <TickIcon />
        </div>
      </Show>
    </div>
  )
}

const langNameMap = {
  ru: 'Русский',
  en: 'English',
}

const langIconMap = {
  ru: <RuLangIcon />,
  en: <EnLangIcon />,
}

const langSelectMap = {
  ru: 'Выбрать язык',
  en: 'Select language',
}

let Settings: Component = () => {
  const [page, setPage] = createSignal('theme')
  const globalCtx = useGlobal()
  const [showSettings, setShowSettings] = globalCtx.showSettings
  const [langName, setLangName] = globalCtx.langId
  const [themeName, setThemeName] = globalCtx.theme
  const lang = globalCtx.lang
  const [selectedLang, setSelectedLang] = createSignal<LanguageId>('ru')

  return (
    <Popup
      show={showSettings()}
      onDismiss={() => setShowSettings(false)}
      class={styles.root}
      showClass={styles.show}
      hideClass={styles.hide}
    >
      <div class={styles.sidebar}>
        <PageSelectButton
          active={page() == 'theme'}
          onClick={() => setPage('theme')}
        >
          {lang.themeTitle}
        </PageSelectButton>
        <PageSelectButton
          active={page() == 'lang'}
          onClick={() => {
            setSelectedLang(langName())
            setPage('lang')
          }}
        >
          {lang.langTitle}: {langNameMap[langName()]} {langIconMap[langName()]}
        </PageSelectButton>
      </div>
      <div class={styles.page}>
        <Switch>
          <Match when={page() == 'theme'}>
            <div class={styles.pageTitle}>{lang.themePage}</div>
            <div class={styles.themeFlex}>
              <For each={Object.keys(ColorThemes)}>
                {(x) => (
                  <ThemeSelectButton
                    active={themeName() == x}
                    setActive={() => setThemeName(x)}
                  >
                    <div
                      class={styles.colorTheme}
                      style={{ 'background-color': x }}
                    />
                  </ThemeSelectButton>
                )}
              </For>

              <ThemeSelectButton
                active={themeName() == 'dark'}
                setActive={() => setThemeName('dark')}
              >
                <img src={themeDarkIcon} />
              </ThemeSelectButton>

              <ThemeSelectButton
                active={themeName() == 'light'}
                setActive={() => setThemeName('light')}
              >
                <img src={themeLightIcon} />
              </ThemeSelectButton>

              <ThemeSelectButton
                active={themeName() == 'anime'}
                setActive={() => setThemeName('anime')}
              >
                <img src={themeAnimeIcon} />
              </ThemeSelectButton>
            </div>
          </Match>
          <Match when={page() == 'lang'}>
            <div class={styles.pageTitle}>{lang.selectLanguage}</div>
            <div class={styles.langSelector}>
              <Radio
                selected={selectedLang() == 'ru'}
                onClick={() => setSelectedLang('ru')}
              >
                <RuLangIcon /> Русский
              </Radio>
              <Radio
                selected={selectedLang() == 'en'}
                onClick={() => setSelectedLang('en')}
              >
                <EnLangIcon /> English
              </Radio>
            </div>
            <Button
              class={styles.langBtn}
              onClick={() => {
                setLangName(selectedLang())
              }}
            >
              {langSelectMap[selectedLang()]}
            </Button>
          </Match>
        </Switch>
      </div>
    </Popup>
  )
}

export default Settings
