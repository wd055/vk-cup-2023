import { ApiUser } from '../../../common/api'
import { Language } from '../../lang'

export function displayName(lang: Language, u: ApiUser): string {
  // console.log('Formating', u)
  if (u.n != '' && u.s != '') return u.n + ' ' + u.s
  if (u.n != '') return u.n
  if (u.s != '') return u.s
  if (u.e == '<self>') return lang.you
  return u.e
}

export function formatTime(date: Date): string {
  return (
    date.getHours().toString().padStart(2, '0') +
    ':' +
    date.getMinutes().toString().padStart(2, '0')
  )
}
