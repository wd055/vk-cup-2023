import {
  Component,
  createEffect,
  createSignal,
  JSX,
  onMount,
  ParentComponent,
  Show,
  splitProps,
} from 'solid-js'
import { Portal } from 'solid-js/web'
import styles from './Popup.module.css'

interface PopupProps {
  show: boolean
  onDismiss: () => void
  showClass?: string
  hideClass?: string
}

const Popup: ParentComponent<
  PopupProps & JSX.HTMLAttributes<HTMLDivElement>
> = (props) => {
  const [p, a] = splitProps(props, [
    'show',
    'onDismiss',
    'class',
    'showClass',
    'hideClass',
    'children',
  ])

  const [render, setRender] = createSignal(false)

  const showStyle = () => p.showClass ?? styles.show
  const hideStyle = () => p.hideClass ?? styles.hide

  const listener = (e: KeyboardEvent) => {
    if (e.key == 'Escape') {
      p.onDismiss()
      e.preventDefault()
    }
  }

  createEffect(() => {
    let shouldAdd = p.show
    if (shouldAdd) document.addEventListener('keydown', listener)
    else document.removeEventListener('keydown', listener)
  })

  return (
    <Show when={p.show || render()}>
      {() => (
        <>
          <div
            class={styles.globalCloser}
            onClick={(e) => {
              p.onDismiss()
              e.stopPropagation()
            }}
          />
          <div
            {...a}
            class={(p.show ? showStyle() : hideStyle()) + ' ' + p.class}
            onAnimationEnd={() => setRender(p.show)}
          >
            {p.children}
          </div>
        </>
      )}
    </Show>
  )
}

export default Popup
