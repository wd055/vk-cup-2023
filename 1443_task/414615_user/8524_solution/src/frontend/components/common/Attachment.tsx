import { Component, createSignal, Show } from 'solid-js'
import styles from './Attachment.module.css'
import DownloadIcon from './assets/download.svg?component-solid'
import { useGlobal } from '../../GlobalContext'

const Attachment: Component<{ id: number }> = (props) => {
  const lang = useGlobal().lang
  const [imageRedy, setImageReady] = createSignal(true)
  return (
    <div class={styles.root}>
      <a href={'/file/' + props.id} download={true}>
        <img
          src={'/file/' + props.id}
          class={styles.img}
          onError={() => setImageReady(true)}
          onLoad={() => setImageReady(false)}
          height={`${imageRedy() ? 0 : 192}px`}
        />
        <Show when={imageRedy()}>
          <div class={styles.failedCont}>{lang.file}</div>
        </Show>
        <div class={styles.download}>
          <DownloadIcon />
          {lang.download}
        </div>
      </a>
    </div>
  )
}

export default Attachment
