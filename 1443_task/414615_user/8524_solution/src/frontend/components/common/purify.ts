const safeAttrs: { [a: string]: string[] } = {
  a: ['href', 'title', 'name', 'style', 'alt'],
  b: ['style'],
  br: ['style'],
  big: ['style'],
  blockquote: ['title', 'style'],
  caption: ['style'],
  code: ['style'],
  del: ['title', 'style'],
  div: ['title', 'style', 'align'],
  dt: ['style'],
  dd: ['style'],
  font: ['color', 'size', 'face', 'style'],
  h1: ['style', 'align'],
  h2: ['style', 'align'],
  h3: ['style', 'align'],
  h4: ['style', 'align'],
  h5: ['style', 'align'],
  h6: ['style', 'align'],
  hr: ['style'],
  i: ['style'],
  img: ['style', 'src', 'alt', 'height', 'width', 'title'],
  ins: ['title', 'style'],
  li: ['style'],
  ol: ['style'],
  p: ['style', 'align'],
  pre: ['style'],
  s: ['style'],
  small: ['style'],
  strong: ['style'],
  span: ['title', 'style', 'align'],
  sub: ['style'],
  sup: ['style'],
  table: ['border', 'width', 'style', 'cellspacing', 'cellpadding'],
  tbody: ['align', 'valign', 'style'],
  td: ['width', 'height', 'style', 'align', 'valign', 'colspan', 'rowspan'],
  tfoot: ['align', 'valign', 'style', 'align', 'valign'],
  th: ['width', 'height', 'style', 'colspan', 'rowspan'],
  thead: ['align', 'valign', 'style'],
  tr: ['align', 'valign', 'style'],
  u: ['style'],
  ul: ['style'],
  title: [],
}

const allowedStyles = [
  'background',
  'background-color',
  'border',
  'border-color',
  'border-width',
  'border-style',
  'border-bottom',
  'border-bottom-color',
  'border-bottom-style',
  'border-bottom-width',
  'border-left',
  'border-left-color',
  'border-left-style',
  'border-left-width',
  'border-right',
  'border-right-color',
  'border-right-style',
  'border-right-width',
  'border-top',
  'border-top-color',
  'border-top-style',
  'border-top-width',
  'display',
  'height',
  'width',
  'color',
  'font',
  'font-family',
  'font-size',
  'font-style',
  'font-variant',
  'font-weight',
  'list-style-type',
  'Table',
  'table-layout',
  'letter-spacing',
  'line-height',
  'text-align',
  'text-decoration',
  'text-indent',
  'text-transform',
  'vertical-align',
  'white-space',
]

function cleanElement(node: HTMLElement | Element) {
  let tagName = node.tagName.toLowerCase()

  if (!safeAttrs.hasOwnProperty(tagName)) {
    console.log('Inavlid', tagName)
    node.remove()
    return
  }
  let allowedAttributes = safeAttrs[tagName]
  Array.from(node.attributes).forEach((e) => {
    if (allowedAttributes.indexOf(e.name) === -1) {
      console.log('Removing', e.name)
      node.removeAttribute(e.name)
      return
    }
    // Clean non http urls
    if (e.name == 'href' || e.name == 'src') {
      if (e.value.indexOf('http') !== 0) {
        node.removeAttribute(e.name)
      }
    }
  })
  if (node instanceof HTMLElement) {
    for (let i = node.style.length; i--; ) {
      const nameString = node.style[i]
      if (allowedStyles.indexOf(nameString) == -1) {
        node.style.removeProperty(nameString)
      } else if (node.style[i].indexOf('var') != 0) {
        node.style.removeProperty(nameString)
      }
    }
  }
}

export function purifyChildren(node: Node) {
  if (node.childNodes) {
    let children = [...node.childNodes]
    children.forEach((n) => purify(n))
  }
}

export function purify(node: Node) {
  if (node.nodeType == Node.ELEMENT_NODE) {
    cleanElement(node as Element)
  }
  purifyChildren(node)
}

export function purifyInto(source: string, target: HTMLElement) {
  try {
    let parser = new DOMParser()
    let test = parser.parseFromString(source, 'text/html')
    let body = test.body
    while (body.firstChild) {
      let node = body.firstChild
      purify(node)
      body.removeChild(node)
      target.appendChild(node)
    }
  } catch (e) {
    console.error(e)
    target.appendChild(document.createTextNode('Invald HTML'))
  }
}
