import { Component, Show } from 'solid-js'
import { Dynamic } from 'solid-js/web'

import Flag0Icon from './assets/flag0.svg?component-solid'
import Flag1Icon from './assets/flag1.svg?component-solid'
import Flag2Icon from './assets/flag2.svg?component-solid'
import Flag3Icon from './assets/flag3.svg?component-solid'
import Flag4Icon from './assets/flag4.svg?component-solid'
import Flag5Icon from './assets/flag5.svg?component-solid'

const IconSelect = [
  Flag0Icon,
  Flag1Icon,
  Flag2Icon,
  Flag3Icon,
  Flag4Icon,
  Flag5Icon,
]

const Category: Component<{ category: number }> = (props) => {
  return <Dynamic component={IconSelect[props.category]} />

}

export default Category
