/* @refresh reload */
import { render } from 'solid-js/web'
import { Router } from '@solidjs/router'

import './index.css'
import App from './App'
import { GlobalProvider } from './GlobalContext'

render(
  () => (
    <GlobalProvider>
      <Router>
        <App />
      </Router>
    </GlobalProvider>
  ),
  document.getElementById('root') as HTMLElement
)
