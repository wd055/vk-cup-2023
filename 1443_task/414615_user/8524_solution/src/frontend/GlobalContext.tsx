import {
  createSignal,
  createContext,
  useContext,
  FlowComponent,
  Context,
  Signal,
  createEffect,
  Accessor,
} from 'solid-js'

import {
  LANGS,
  Language,
  LanguageEntry,
  LanguageId,
  LANG_EN,
  LANG_RU,
} from './lang'
import { ColorThemes } from './theme'

interface IGlobalContext {
  folder: Signal<number>
  headerToolRef: Signal<Node>
  showSettings: Signal<boolean>
  showNewLetter: Signal<boolean>

  langId: Signal<LanguageId>
  lang: Language

  theme: Signal<string>
}

const defaultNode: Node = document.createElement('div')
const defaultContext: IGlobalContext = {
  folder: createSignal(-1),
  headerToolRef: createSignal(defaultNode),
  showSettings: createSignal(false),
  showNewLetter: createSignal(false),

  langId: createSignal('ru'),
  lang: LANG_RU,

  theme: createSignal(''),
}

const GlobalContext: Context<IGlobalContext> = createContext(defaultContext)

export const GlobalProvider: FlowComponent = (props) => {
  const headerToolRef = createSignal(defaultNode)
  const folder = createSignal(-1)
  const showSettings = createSignal(false)
  const showNewLetter = createSignal(false)

  const [langName, setLangName] = createSignal<LanguageId>(
    (localStorage.getItem('lang') as LanguageId) ?? 'ru'
  )

  createEffect(() => {
    localStorage.setItem('lang', langName())
  })

  // Dynamic language proxy
  const lang: Language = Object.defineProperties(
    {},
    Object.fromEntries(
      (Object.keys(LANG_EN) as LanguageEntry[]).map((entry) => [
        entry,
        {
          get: () => LANGS[langName()][entry],
        },
      ])
    )
  ) as Language

  const [theme, setTheme] = createSignal(
    localStorage.getItem('theme') ?? 'light'
  )

  createEffect(() => {
    localStorage.setItem('theme', theme())

    if (theme()[0] == '#') {
      document.documentElement.className = `theme-base`
      // console.log(ColorThemes[theme()])
      if (ColorThemes[theme()][1]) {
        document.documentElement.className += ` theme-lightcolor`
      }

      document.documentElement.style.setProperty('--page-bg', theme())
      document.documentElement.style.setProperty(
        '--header-color',
        ColorThemes[theme()][0]
      )
    } else {
      document.documentElement.className = `theme-base theme-${theme()}`
      document.documentElement.style.removeProperty('--page-bg')
      document.documentElement.style.removeProperty('--header-color')
    }
  })

  return (
    <GlobalContext.Provider
      value={{
        folder,
        headerToolRef,
        showSettings,
        showNewLetter,
        theme: [theme, setTheme],

        langId: [langName, setLangName],
        lang,
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  )
}

export function useGlobal() {
  return useContext(GlobalContext)
}
