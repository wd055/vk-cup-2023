export type LanguageId = 'en' | 'ru'

export const LANG_EN = {
  // Sidebar
  newLetter: 'New letter',
  settings: 'Settings',
  menu: 'Menu',
  folderNames: [
    'Inbox',
    'Important',
    'Sent',
    'Drafts',
    'Archive',
    'Spam',
    'Trash',
  ],
  newFolder: 'New folder',
  createFolder: 'Create folder',

  // Settings
  themeTitle: 'Appearance',
  langTitle: 'Language',

  themePage: 'Customize your email appearance and theme',
  selectLanguage: 'Select Language',

  // Message
  today: 'Today',
  yesterday: 'Yesterday',
  dateLocale: 'en-US',
  you: 'You',
  mailTo: 'To',
  recipientsPrefix: '',
  recipientsPostfix: 'more recipients',
  file: 'File',
  download: 'Download',
  flag: [
    'Orders',
    'Finance',
    'Registrations',
    'Trips',
    'Tickets',
    'Fines and taxes',
  ],

  // Header
  back: 'Back',

  // Folder
  filter: 'Filter',
  filters: 'Filters',
  unread: 'Unread',
  marked: 'Marked',
  attach: 'With attachments',
  allLet: 'All letters',

  emptyFolder: 'No letters',

  // New letter
  subject: 'Subject',
  toWho: 'To',
  oneRecipientErr: 'Enter at least one recipient',
  send: "Send",
  atLeastSmth: "Write something",
  htmlMode: "HTML mode",
  link: "Link",
  text: "Text",
  ceateLink: "Create link",

  // Folder ops
  unreadOp: "Mark unread",
  readOp: "Mark read",
  bookmarkOp: "Add mark",
  unbookmarkOp: "Remove mark",
  moveTo: "Move to",
}

export type LanguageEntry = keyof typeof LANG_EN
export type Language = typeof LANG_EN

export const LANG_RU: Language = {
  // Sidebar
  newLetter: 'Написать письмо',
  settings: 'Настройки',
  menu: 'Меню',
  folderNames: [
    'Входящие',
    'Важное',
    'Отправленные',
    'Черновики',
    'Архив',
    'Спам',
    'Корзина',
  ],
  newFolder: 'Новая папка',
  createFolder: 'Создать папку',

  // Settings
  themeTitle: 'Внешний вид',
  langTitle: 'Язык',

  themePage: 'Настройки внешнего вида вашей почты и темы оформления',
  selectLanguage: 'Изменить язык',

  // Message
  today: 'Сегодня',
  yesterday: 'Вчера',
  dateLocale: 'ru-RU',
  you: 'Вы',
  mailTo: 'Кому',
  recipientsPrefix: 'ещё ',
  recipientsPostfix: 'получателя',

  file: 'Файл',
  download: 'Скачать',

  flag: [
    'Заказы',
    'Финансы',
    'Регистрации',
    'Путешествия',
    'Билеты',
    'Штрафы и налоги',
  ],

  // Header
  back: 'Вернуться',

  // Folder
  filter: 'Фильтр',
  filters: 'Фильтры',
  unread: 'Непрочитанные',
  marked: 'С флажком',
  attach: 'С вложениями',
  allLet: 'Все письма',

  emptyFolder: 'Писем нет',

  // New letter
  subject: 'Тема',
  toWho: 'Кому',
  oneRecipientErr: 'Укажите хотя бы одного получателя',
  send: "Отправить",
  atLeastSmth: "Напишите что-нибудь",
  htmlMode: "Режим HTML",
  link: "Ссылка",
  text: "Текст",
  ceateLink: "Создать ссылку",


  unreadOp: "Непрочитано",
  readOp: "Прочитано",
  bookmarkOp: "Пометить",
  unbookmarkOp: "Убрать пометку",
  moveTo: "Переместить в",
}

export const LANGS = {
  ru: LANG_RU,
  en: LANG_EN,
}
