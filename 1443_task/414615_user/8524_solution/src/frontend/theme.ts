export const ColorThemes: { [a: string]: [string, boolean] } = {
  '#4A352F': ['#322420', false],
  '#424242': ['#2D2D2D', false],
  '#5A355A': ['#3D243D', false],
  '#35385A': ['#24263D', false],
  '#646ECB': ['#444B8A', false],
  '#E73672': ['#9D254E', false],
  '#F44336': ['#A62E25', false],
  '#388E3C': ['#266129', false],

  '#81d8d0': ['#58938d', true],
  '#e2dcd2': ['#9a968f', true],
  '#ffebcd': ['#ada08b', true],
  '#e7eed2': ['#9da28f', true],
  '#d0f0f7': ['#8da3a8', true],
  '#c9d0fb': ['#898dab', true],
  '#ddf3ff': ['#96a5ad', true],
  '#f0f0f0': ['#a3a3a3', true],
}
