var p=class{constructor(){this.events={}}subscribe(t,o){let r=this;return r.events.hasOwnProperty(t)||(r.events[t]=[]),r.events[t].push(o)}publish(t,o={}){let r=this;return r.events.hasOwnProperty(t)?r.events[t].map(l=>l(o)):[]}};var g=class{constructor(t){let o=this;o.actions={},o.mutations={},o.state={},o.status="resting",o.events=new p,t.hasOwnProperty("actions")&&(o.actions=t.actions),t.hasOwnProperty("mutations")&&(o.mutations=t.mutations),o.state=new Proxy(t.state||{},{set:function(r,l,n){return r[l]===n?(o.status="resting",!0):(r[l]=n,console.log(`stateChange: ${l}: ${n}`),o.events.publish("stateChange",o.state),o.status!=="mutation"&&console.warn(`You should use a mutation to set ${l}`),o.status="resting",!0)}})}async dispatch(t,o){let r=this;return typeof r.actions[t]!="function"?(console.error(`Action "${t} doesn't exist.`),!1):(r.status="action",await r.actions[t](r,o),!0)}async commit(t,o){let r=this;if(typeof r.mutations[t]!="function")return console.log(`Mutation "${t}" doesn't exist`),!1;r.status="mutation";let l=await r.mutations[t](r.state,o);return r.state=Object.assign(r.state,l),!0}};var s=class{constructor(t={}){let o=this;this.init=this.init||function(){},this.render=this.render||function(){},t.store instanceof g&&t.store.events.subscribe("stateChange",()=>o.render()),t.hasOwnProperty("element")&&(this.element=t.element)}};var Y={"Content-Type":"application/json"},U=e=>e.ok?e.json():e.json().then(t=>Promise.reject(t)),C=async(e={})=>{let t=new URLSearchParams(e).toString(),o=await fetch(`/api/letters/?${t}`,{method:"GET",headers:Y});return U(o)};var S={async lettersChange(e,t={page:1,limit:20}){let o=e.state.filters;o=Object.keys(o).filter(l=>o[l]);let r=await C({folder:e.state.currentFolder,...o.reduce((l,n)=>(n=n.split("."),{...l,[n[0]]:n[1]}),{}),...t});e.commit("pageChange",t.page),e.commit("lettersChange",r.results)},async loadMore(e){let t=e.state.page+1,o=e.state.limit;this.lettersChange(e,{page:t,limit:o})},letterChange(e,t={letter:null}){e.commit("letterChange",t.letter)},locationChange(e,t={location:"list"}){e.commit("locationChange",t.location)},async folderChange(e,t={folder:"__all__"}){e.commit("folderChange",t.folder),this.locationChange(e,{location:"list",_id:null}),await this.lettersChange(e)},async filterChange(e,t={filter:"__all__"}){e.commit("filterChange",t.filter),await this.lettersChange(e)},async languageChange(e,t={}){let r=(await import(`../config/lang/${document.querySelector('#settings-language input[type="radio"]:checked').value}.js`)).default;e.commit("languageChange",r)}};var $={async lettersChange(e,t){return e.letters=t,e},letterChange(e,t){return e.currentLetter=t,e},locationChange(e,t){return e.currentLocation=t,e},folderChange(e,t){return e.currentFolder=t,e},pageChange(e,t){return e.page=t,e},filterChange(e,t){switch(t){case"__all__":case"__reset__":{e.filters={"read.false":!1,"bookmark.true":!1,"doc.__null__":!1};break}default:{e.filters[t]=!e.filters[t];break}}return console.log(e.filters),e},languageChange(e,t){return e.langConfig=t,e}};var L={currentLocation:"list",currentLetter:null,letters:[],page:1,limit:15,filters:{"read.false":!1,"bookmark.true":!1,"doc.__null__":!1},langConfig:{}};var a=new g({actions:S,mutations:$,state:L});var _=(e,t="ru-RU")=>{let o={day:"numeric",month:"numeric",year:"2-digit"};e=new Date(e);let r=new Date;return e.getFullYear()===r.getFullYear()&&(e.getMonth()===r.getMonth()&&e.getDate()===r.getDate()?o={hour:"numeric",minute:"numeric"}:o={month:"short",day:"numeric"}),e=e.toLocaleString(t,o),e},q=(e,t="ru-RU")=>{console.log(t);let o={day:"numeric",month:"long",year:"numeric",hour:"numeric",minute:"numeric"};e=new Date(e);let r=new Date;return e.getFullYear()===r.getFullYear()&&(e.getMonth()===r.getMonth()&&e.getDate()===r.getDate()?o={weekday:"long",hour:"numeric",minute:"numeric"}:o={day:"numeric",month:"long",hour:"numeric",minute:"numeric"}),e=e.toLocaleString(t,o),e};var D=e=>e.length*.75-2,B=(e,t)=>e.toPrecision(t);var T=e=>e.getAttributeNames().reduce((t,o)=>({...t,[o]:e.getAttribute(o)}),{}),c=(e,t,o,r="")=>{if(e instanceof Element)return t?e.setAttribute(o,r):e.removeAttribute(o),e};var d=class extends s{constructor(){super({store:a,element:document.querySelectorAll("[data-action]")})}static onClick(t){if(t.target.hasAttribute("skip")){console.log(t),t.stopPropagation();return}let o=this.getAttribute("data-action"),r=T(this);a.dispatch(o,r)}init(){this.element.forEach(function(t){t.addEventListener("click",d.onClick,!1)})}};var h=class extends s{constructor(){super({store:a,element:document.querySelector("#list")}),this.parent=document.querySelector("#root > main")}init(){a.dispatch("folderChange",{folder:"__all__"}),this.parent.addEventListener("scroll",t=>{if(a.state.currentLocation!=="list")return;t.target.scrollHeight-t.target.scrollTop<=t.target.clientHeight&&(document.querySelector("#loading-list").removeAttribute("hidden"),a.dispatch("loadMore"))})}render(){a.state.page===1&&(this.element.innerHTML="",this.parent.scrollTop=0),c(this.element,a.state.currentLocation!=="list","hidden"),a.state.currentLocation==="list"&&(document.querySelector("#loading-list").setAttribute("hidden",""),a.state.letters.forEach(t=>{let o=`
            <button
                location="view"
                data-action="locationChange"
                class="letter"
            >
                <div class="unread-dot-box">
                    <div read="${t.read}" class="read-dot"></div>
                </div>

                <div class="avatar">
                    <input id="checkbox-${t.id}" type="checkbox" class="checkbox" hidden skip/>
                    <label for="checkbox-${t.id}" class="checkbox-label" skip>
                        <i class="icon-check" skip></i>
                    </label>
                    <img 
                        src=${t.author.avatar||"assets/default-avatar.png"}
                        alt="avatar"
                    />
                </div>

                <div class="letter-content-wrapper">
                    <div class="letter-content">
                        <div read="${t.read}" class="author-name">
                            <span class="text text-overflow text-color-primary">
                                ${t.author.name} ${t.author.surname}
                            </span>
                        </div>
                        <div class="icon-flag">
                            <span class="ico ico-20">
                                ${t.important?'<i class="icon-exclamation-mark red"></i>':t.bookmark?'<i class="icon-bookmark-solid red"></i>':'<i class="icon-bookmark-regular gray" invisible></i>'}
                            </span>
                        </div>
                        <div class="message-box text-overflow">
                            <p
                                read="${t.read}"
                                class="message-title text text-overflow text-color-primary"
                            >
                                ${t.title}
                            </p>
                            <p class="text text-overflow text-color-secondary">
                                ${t.text}
                            </p>
                        </div>
                        <div class="icons">
                            ${"flag"in t?`<span class="ico ico-24">
                                    <i class="icon-${t.flag.toLowerCase().replace(/ /g,"-")}"></i>
                                </span>`:""}
                            ${"doc"in t?`<span class="doc-preview-button ico ico-24" tabindex="0" skip>
                                    <i class="icon-attachment text-color-primary" skip></i>
                                </span>`:""}
                        </div>
                        <p class="text text-color-secondary">
                            ${_(t.date)}
                        </p>
                    </div>
                    <div class="divider"></div>
                </div>
            </button>`;this.element.insertAdjacentHTML("beforeend",o);let r=this.element.querySelector("button:last-child");r.addEventListener("click",d.onClick,!1),r.addEventListener("click",function(l){a.dispatch("letterChange",{letter:t})},!1)}),c(document.querySelector("#empty-list"),a.state.letters.length!=0,"hidden"))}};var b=class extends s{constructor(){super({store:a,element:document.querySelector("#view")})}render(){if(this.element.innerHTML="",a.state.currentLocation!=="view")return;let t=a.state.currentLetter;t&&(this.element.innerHTML=`
        <header class="view-header">
            <h1 class="text-weight-bold">
                ${t.title}
            </h1>

            ${"flag"in t?`<div
                    class="view-category-content-wrapper"
                >
                    <div class="view-category-content">
                        <span class="ico ico-24">
                            <i class="icon-${t.flag.toLowerCase().replace(/ /g,"-")}"></i>
                        </span>
                        <span
                            class="text text-color-primary"
                            data-i18n="view.flag-${t.flag.toLowerCase()}"
                        >v
                            ${t.flag}
                        </span>
                    </div>
                </div>`:""}
        </header>
        <div class="view-head">
            <div class="unread-dot">
                <div read="${t.read}" class="read-dot"></div>
            </div>
            <div class="view-head-content">
                <div class="avatar">
                    <img 
                        src=${t.author.avatar||"assets/default-avatar.png"}
                        alt="avatar"
                    />
                </div>
                <div class="view-head-info">
                    <div class="view-head-info--title">
                        <p class="text text-color-primary">
                            ${t.author.name} ${t.author.surname}
                        </p>
                        <p class="date text text-type-footnote text-color-secondary">
                            ${q(t.date)}
                        </p>
                        <span class="ico ico-20">
                            ${t.important?'<i class="icon-exclamation-mark red"></i>':t.bookmark?'<i class="icon-bookmark-solid red"></i>':""}
                        </span>
                    </div>
                    <p class="text text-type-footnote text-color-secondary">
                        <span data-i18n="view.to-whom">\u041A\u043E\u043C\u0443: \u0412\u044B</span>
                        ${t.to.slice(0,3).map(o=>`<span>, ${o.name} ${o.surname}</span>`).join("")}
                        ${t.to.length>3?`<span class="text-underline">
                                <span data-i18n="view.and">\u0435\u0449\u0435</span> ${t.to.length-3} <span data-i18n="view.recipients">\u043F\u043E\u043B\u0443\u0447\u0430\u0442\u0435\u043B\u0435\u0439</span> 
                            </span>`:""}
                    </p>
                </div>
            </div>
        </div>
        ${"doc"in t?`<div ${"doc"in t?"":"hidden"} class="view-attaches">
                <div class="view-attaches-content">
                    <div class="view-images">
                        ${Object.entries(t.doc).map(([o,r])=>`
                                    <div class="view-image">
                                        <img src="${r}" alt="image" />
                                        <div class="view-image-download text-color-primary">
                                            <span class="ico ico-16">
                                                <i class="icon-download"></i>
                                            </span>
                                            <a
                                                href="#"
                                                class="text"
                                                data-i18n="view.download-button"
                                            >
                                                \u0421\u043A\u0430\u0447\u0430\u0442\u044C
                                            </a>
                                        </div>
                                    </div>
                                `).join("")}
                    </div>
                    <p class="view-action text text-type-footnote">
                        <span>
                            ${Object.keys(t.doc).length} <span data-i18n="view.files">\u0444\u0430\u0439\u043B(\u0430)</span>
                        </span>
                        <span>
                            <a
                                href="#"
                                class="text-color-select"
                                data-i18n="view.download-button"
                            >
                                \u0421\u043A\u0430\u0447\u0430\u0442\u044C
                            </a>
                            &nbsp;
                            <span class="text-color-secondary">
                                (${B(Object.values(t.doc).reduce((o,r)=>o+D(r),0)/1e6,2)}) Mb
                            </span>
                        </span>
                    </p>
                </div>`:""}
        </div>
        <div class="view-body">
            <p class="text text-type-letter text-color-primary">
                ${t.text}
            </p>
        </div>`)}};var f=class extends s{constructor(){super({store:a,element:document.querySelector("#header .header-content")})}render(){let t=this.element.querySelector("#back-button"),o=this.element.querySelector("#logo-box"),r=this.element.querySelector("#filter-button");c(t,a.state.currentLocation!=="view","hidden"),c(o,a.state.currentLocation==="view","hidden"),c(r,a.state.currentLocation==="view","hidden")}};var v=class extends s{constructor(){super({store:a,element:document.querySelector("#sidebar .folders")})}init(){document.querySelector("#compose-button").addEventListener("click",t=>{document.querySelector("#compose-window").removeAttribute("hidden")},!1),document.querySelector("#wysiwyg-close-button").addEventListener("click",t=>{document.querySelector("#compose-window").setAttribute("hidden","")},!1)}render(){let t=a.state.currentFolder,o=this.element.querySelector("button[selected]");o&&o.removeAttribute("selected"),this.element.querySelector(`[folder="${t}"]`).setAttribute("selected","")}};var y=class extends s{constructor(){super({store:a,element:document.querySelector("#filter-button")})}render(){let t={...a.state.filters};t.__all__=Object.values(t).every(i=>!i);let o=0,r=null;Object.entries(t).forEach(([i,u])=>{let m=this.element.querySelector(`.select-item[filter="${i}"] .ico`),E=this.element.querySelector(`.ico[type="${i}"]`);c(m,!u,"invisible"),c(E,!u,"hidden"),o+=u,r=u?i:r});let l=this.element.querySelector("#reset-filters");c(l,t.__all__,"hidden");let n=this.element.querySelector("#filter-text");if(o>=2)n.setAttribute("data-i18n","header.filter-button-plural");else{if(r=="__all__"){n.setAttribute("data-i18n","header.filter-button");return}let i=this.element.querySelector(`.ico[type="${r}"]`);n.setAttribute("data-i18n",i.getAttribute("text-i18n"))}}};var k=class extends s{constructor(){super({store:a})}init(){a.dispatch("languageChange",{})}render(){document.querySelectorAll("[data-i18n]").forEach(o=>{let l=o.getAttribute("data-i18n").split(".").reduce((n,i)=>n[i]??{},a.state.langConfig);l&&(o.innerHTML=l)});let t=a.state.langConfig.lang??"ru";document.querySelectorAll(".country-flag-img").forEach(o=>{o.src=`/assets/icons/flags/${t}.png`,o.alt=t})}};var j={"dark-brown":{"data-theme":"white",styles:{"--color-background-header":"#291B18","--color-background":"#4E342E","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"dark-grey":{"data-theme":"white",styles:{"--color-background-header":"#242424","--color-background":"#424242","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"dark-purple":{"data-theme":"white",styles:{"--color-background-header":"#331D33","--color-background":"#5A355A","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"deep-blue":{"data-theme":"white",styles:{"--color-background-header":"#171928","--color-background":"#35385a","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},lavender:{"data-theme":"white",styles:{"--color-background-header":"#434C98","--color-background":"#646ECB","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"hot-pink":{"data-theme":"white",styles:{"--color-background-header":"#B31C4F","--color-background":"#E73672","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},orange:{"data-theme":"white",styles:{"--color-background-header":"#B73026","--color-background":"#F44336","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},green:{"data-theme":"white",styles:{"--color-background-header":"#27662A","--color-background":"#388E3C","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.24)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"light-cyan":{"data-theme":"white",styles:{"--color-background-header":"#3C928A","--color-background":"#81D8D0","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},beige:{"data-theme":"white",styles:{"--color-background-header":"#837E78","--color-background":"#E2DCD5","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},skin:{"data-theme":"white",styles:{"--color-background-header":"#8C847E","--color-background":"#FFEBDC","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"light-green":{"data-theme":"white",styles:{"--color-background-header":"#747A63","--color-background":"#E7EED2","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"light-blue":{"data-theme":"white",styles:{"--color-background-header":"#556F74","--color-background":"#D0F0F7","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"light-purple":{"data-theme":"white",styles:{"--color-background-header":"#5E5E76","--color-background":"#C9D0FB","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"snow-blue":{"data-theme":"white",styles:{"--color-background-header":"#535F6B","--color-background":"#DDF3FF","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}},"gray-white":{"data-theme":"white",styles:{"--color-background-header":"#808080","--color-background":"#F0F0F0","--color-filter-text":"#E7E8EA","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(0, 16, 61, 0.04)","--color-button-background-alpha--active":"rgba(0, 16, 61, 0.08)","--color-logo-text":"#FFFFFF","--background-img":"none","--background-img-overlay":"none","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}}};var M={"light-mode":{name:"Light theme","data-theme":"white",styles:{"--color-background-header":"#FFFFFF","--color-background":"#F6F7F8","--color-background--hover":"#E6E8EC","--color-background--active":"#DDDFE4","--background-img":"none","--background-img-overlay":"none","--preview-img":"url('assets/themes/light-mode-preview.png')"}},"dark-mode":{name:"Dark mode","data-theme":"black",styles:{"--color-background-header":"#232324","--color-background":"#19191A","--color-background--hover":"#222223","--color-background--active":"#2B2B2C","--background-img":"none","--background-img-overlay":"none","--preview-img":"url('assets/themes/dark-mode-preview.png')"}},anime:{name:"Anime","data-theme":"white",styles:{"--color-background-header":"#750045","--color-background":"#FF0F9D","--color-logo-text":"#FFFFFF","--color-filter-text":"#E7E8EA","--color-button-text":"#E7E8EA","--color-button-text--hover":"#E8E9EB","--color-button-text--active":"#E9EAEC","--color-button-background-alpha":"transparent","--color-button-background-alpha--hover":"rgba(255, 255, 255, 0.12)","--color-button-background-alpha--active":"rgba(255, 255, 255, 0.24)","--background-img":"url('../assets/themes/anime-background.png')","--background-img-overlay":"linear-gradient(-180deg, #750045a3 0%, #000953a3 100%)","--preview-img":"url('assets/themes/anime-preview.png')","--empty-list-img":"url(../assets/empty-list-custom.png)","--empty-list-width":"40px","--empty-list-height":"40px"}}};var x=class extends s{constructor(){super({store:a,element:document.querySelector("#settings-theme")})}populateThemeList(t,o){t.innerHTML="";for(let r in o){let l=`
            <input
                type="radio"
                id="${r}"
                value="${r}"
                name="color-theme"
                hidden
            />
            <label for="${r}" class="theme-box">
                <div
                    class="theme"
                    style="
                        background-color: ${o[r].styles["--color-background"]};
                        background-image: ${o[r].styles["--preview-img"]||"none"};
                    "
                ></div>
                <div class="selected-theme">
                    <span class="ico ico-24">
                        <img src="assets/icons/check-big.svg" alt="Selected" />
                    </span>
                </div>
                <div
                    class="hover-theme"
                >
                    <span
                        class="text"
                        data-i18n="settings.theme.${r}"
                        ${"name"in o[r]?"":"hidden"}
                    >
                        Theme name
                    </span>
                </div>
            </label>
            `;t.insertAdjacentHTML("beforeend",l);let n=t.querySelector(`#${r}`);n&&n.addEventListener("change",()=>{let i=document.querySelector("body");i.style="";for(let u in o[r].styles)i.style.setProperty(u,o[r].styles[u]);i.setAttribute("data-theme",o[r]["data-theme"])})}}init(){this.populateThemeList(this.element.querySelector(".color-theme-box"),j),this.populateThemeList(this.element.querySelector(".custom-theme-box"),M),this.element.querySelector("#light-mode").setAttribute("checked","")}};var F=document.querySelector("#wysiwyg-editor"),O=F.querySelectorAll(".toolbar [data-command]"),w=F.querySelector(".content-area .visual-view"),P=0;O.forEach(e=>{e.addEventListener("click",t=>{w.focus(),G(w,P);let o=e.dataset.command,r=e.dataset.value||"";document.execCommand(o,!1,r)},!1)});function z(e){var t=window.getSelection().getRangeAt(0),o=t.cloneRange();o.selectNodeContents(e),o.setEnd(t.startContainer,t.startOffset);var r=o.toString().length;return{start:r,end:r+t.toString().length}}function G(e,t){var o=0,r=document.createRange();r.setStart(e,0),r.collapse(!0);for(var l=[e],n,i=!1,u=!1;!u&&(n=l.pop());)if(n.nodeType==3){var m=o+n.length;!i&&t.start>=o&&t.start<=m&&(r.setStart(n,t.start-o),i=!0),i&&t.end>=o&&t.end<=m&&(r.setEnd(n,t.end-o),u=!0),o=m}else for(var E=n.childNodes.length;E--;)l.push(n.childNodes[E]);var A=window.getSelection();A.removeAllRanges(),A.addRange(r)}w.addEventListener("input",e=>{console.log("hello",e),P=z(w),O.forEach(o=>{o.checked=!1});let t=window.getSelection().anchorNode.parentNode;if(!V(t,F))return!1;R(t)});function V(e,t){return t.contains(e)}function R(e){if(!e||!e.classList||e.classList.contains("visual-view"))return!1;let t,o=e.tagName.toLowerCase();t=document.querySelectorAll(`.toolbar [data-tag-name="${o}"]`)[0],t&&(t.checked=!0);let r=e.style.textAlign;return t=document.querySelectorAll(`.toolbar [data-style="align:${r}"]`)[0],t&&(t.checked=!0),R(e.parentNode)}var W=new d,H=new h,J=new b,Q=new f,I=new v,X=new y,N=new k,Z=new x;H.init();W.init();I.init();N.init();Z.init();Q.render();H.render();I.render();X.render();J.render();N.render();
