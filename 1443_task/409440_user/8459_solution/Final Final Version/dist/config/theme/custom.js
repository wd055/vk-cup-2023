export default {
    "light-mode": {
        "name": "Light theme",

        "data-theme": "white",
        "styles": {
            "--color-background-header": "#FFFFFF",

            "--color-background": "#F6F7F8",
            "--color-background--hover": "#E6E8EC",
            "--color-background--active": "#DDDFE4",

            "--background-img": "none",
            "--background-img-overlay": "none",
            "--preview-img": "url('assets/themes/light-mode-preview.png')",
        }
    },
    "dark-mode": {
        "name": "Dark mode",

        "data-theme": "black",
        "styles": {
            "--color-background-header": "#232324",

            "--color-background": "#19191A",
            "--color-background--hover": "#222223",
            "--color-background--active": "#2B2B2C",

            "--background-img": "none",
            "--background-img-overlay": "none",
            "--preview-img": "url('assets/themes/dark-mode-preview.png')",
        }
    },
    "anime": {
        "name": "Anime",

        "data-theme": "white",
        "styles": {
            "--color-background-header": "#750045",
            "--color-background": "#FF0F9D",

            "--color-logo-text": "#FFFFFF",

            "--color-filter-text": "#E7E8EA",

            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.12)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--background-img": "url('../assets/themes/anime-background.png')",
            "--background-img-overlay": "linear-gradient(-180deg, #750045a3 0%, #000953a3 100%)",
            "--preview-img": "url('assets/themes/anime-preview.png')",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    }
}