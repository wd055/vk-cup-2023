const fs = require("fs");
const http = require("http");
const path = require("path");

const controllers = require("./js/api/controller.js");

const PORT = 3000;

const MIME_TYPES = {
    default: 'application/octet-stream',
    html: 'text/html; charset=UTF-8',
    js: 'application/javascript; charset=UTF-8',
    css: 'text/css',
    png: 'image/png',
    jpg: 'image/jpg',
    gif: 'image/gif',
    ico: 'image/x-icon',
    svg: 'image/svg+xml',
};

const toBool = [() => true, () => false];

const prepareFile = async (url) => {
    const paths = [__dirname, url];

    if (url.endsWith('/')) {
        paths.push('index.html');
    }

    const filePath = decodeURI(path.join(...paths));
    const pathTraversal = !filePath.startsWith(__dirname);
    const exists = await fs.promises.access(filePath).then(...toBool);

    const found = !pathTraversal && exists;
    const streamPath = filePath;
    const ext = path.extname(streamPath).substring(1).toLowerCase();
    const stream = fs.createReadStream(streamPath);

    return { found, ext, stream };
};

const server = http.createServer(async (req, res) => {   
    req.url = decodeURIComponent(req.url);
    
    if (req.url.substring(1, 4) === "api") {
        // for /letters
        if (req.url.substring(5, 12) !== "letters") {
            res.writeHead(404, { 'Content-Type': 'application/json' });
            res.write(JSON.stringify({ message: "Not found" }));
            res.end();

            console.log(`${req.method} ${req.url} ${res.statusCode}`);
            return;
        }

        for (const middleware of controllers.getLettersList) {
            ({req, res} = await middleware(req, res));

            if (res.statusCode !== 200) {
                console.log(`${req.method} ${req.url} ${res.statusCode}`);
                return;
            }
        }

        return;
    } else {
        const file = await prepareFile(req.url);
        const statusCode = file.found ? 200 : 404;
        const mimeType = MIME_TYPES[file.ext] || MIME_TYPES.default;

        res.writeHead(statusCode, { 'Content-Type': mimeType });
        file.stream.pipe(res);
        
        console.log(`${req.method} ${req.url} ${statusCode}`);
    }

});


server.listen(PORT, function(error){
    if (error) {
        console.log("Something went wrong!", error)
    } else{
        console.log("Server is listening on port "+ PORT)
    }
});
