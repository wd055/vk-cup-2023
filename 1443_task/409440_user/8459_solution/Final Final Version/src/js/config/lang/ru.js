export default {
    "lang": "ru",

    "header": {
        "back-button": "Вернуться",

        "filter-button": "Фильтр",
        "filter-button-plural": "Фильтры",
        
        "filter-all": "Все письма",
        "filter-unread": "Непрочитанные",
        "filter-flagged": "С флажком",
        "filter-doc": "С вложениями",
        "filter-reset": " Сбросить всё"
    },
    "sidebar": {
        "compose-button": "Написать письмо",
        
        "folder-all": "Входящие",
        "folder-important": "Важное",
        "folder-sent": "Отправленные",
        "folder-drafts": "Черновики",
        "folder-archive": "Архив",
        "folder-spam": "Спам",
        "folder-trash": "Корзина",
        "new-folder-button": "Новая папка",
    },
    "settings": {
        "settings-button": "Настройки",

        "theme-button": "Внешний вид",
        "language-button": "Язык: Русский",

        "theme-header": "Настройки внешнего вида вашей почты и темы оформления",

        "language-header": "Изменить язык",
        "language-apply-button": "Выбрать язык",

        "theme": {
            "light-mode": "Светлая тема",
            "dark-mode": "Тёмная тема",
            "anime": "Аниме"
        }
    },
    "view": {
        "to-whom": "Кому: Вы",
        "and": "еще",
        "recipients": "получателей",

        "flag-билеты": "Билеты",
        "flag-заказы": "Заказы",
        "flag-путешевствия": "Путешествия",
        "flag-регистрации": "Регистрации",
        "flag-финансы": "Финансы",
        "flag-штрафы и налоги": "Штрафы и налоги",

        "download-button": "Скачать",
        "files": "файл(а)",
    },
    "empty-list": {
        "empty-list-header": "Писем нет",
    },
    "wysiwyg": {
        "header": "Новое письмо",
        "toolbar": {
            "paragraph": "Обычный текст",
            "h1": "Заголовок 1",
            "h2": "Заголовок 2",
            "h3": "Заголовок 3",
            "justify-left": "По левому краю",
            "justify-center": "По центру",
            "justify-right": "По правому краю",
            "outdent": "Уменьшить отступ",
            "indent": "Увеличить отступ",
            "ordered-list": "Нумерованный",
            "unordered-list": "Маркированный",
        },
        "send-button": "Отправить",
        "cancel-button": "Отменить",
        "save-button": "Сохранить",
    }
}