export default {
    "dark-brown": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#291B18",
            "--color-background": "#4E342E",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "dark-grey": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#242424",
            "--color-background": "#424242",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "dark-purple": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#331D33",
            "--color-background": "#5A355A",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "deep-blue": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#171928",
            "--color-background": "#35385a",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "lavender": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#434C98",
            "--color-background": "#646ECB",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",
            
            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "hot-pink": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#B31C4F",
            "--color-background": "#E73672",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "orange": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#B73026",
            "--color-background": "#F44336",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "green": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#27662A",
            "--color-background": "#388E3C",

            "--color-filter-text": "#E7E8EA",
            "--color-button-text": "#E7E8EA",
            "--color-button-text--hover": "#E8E9EB",
            "--color-button-text--active": "#E9EAEC",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(255, 255, 255, 0.24)",
            "--color-button-background-alpha--active": "rgba(255, 255, 255, 0.24)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "light-cyan": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#3C928A",
            "--color-background": "#81D8D0",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "beige": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#837E78",
            "--color-background": "#E2DCD5",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "skin": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#8C847E",
            "--color-background": "#FFEBDC",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "light-green": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#747A63",
            "--color-background": "#E7EED2",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "light-blue": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#556F74",
            "--color-background": "#D0F0F7",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "light-purple": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#5E5E76",
            "--color-background": "#C9D0FB",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "snow-blue": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#535F6B",
            "--color-background": "#DDF3FF",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    },
    "gray-white": {
        "data-theme": "white",
        "styles": {
            "--color-background-header": "#808080",
            "--color-background": "#F0F0F0",

            "--color-filter-text": "#E7E8EA",
            "--color-button-background-alpha": "transparent",
            "--color-button-background-alpha--hover": "rgba(0, 16, 61, 0.04)",
            "--color-button-background-alpha--active": "rgba(0, 16, 61, 0.08)",

            "--color-logo-text": "#FFFFFF",

            "--background-img": "none",
            "--background-img-overlay": "none",

            "--empty-list-img": "url(../assets/empty-list-custom.png)",
            "--empty-list-width": "40px",
            "--empty-list-height": "40px",
        }
    }
}