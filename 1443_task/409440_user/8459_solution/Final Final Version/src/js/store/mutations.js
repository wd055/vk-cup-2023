export default {
    async lettersChange(state, payload) {
        state.letters = payload;

        return state;
    },
    letterChange(state, payload) {
        state.currentLetter = payload;

        return state;
    },
    locationChange(state, payload) {
        state.currentLocation = payload;

        return state;
    },
    folderChange(state, payload) {
        state.currentFolder = payload;

        return state;
    },
    pageChange(state, payload) {
        state.page = payload;

        return state;
    },
    filterChange(state, payload) {
        switch(payload) {
            case "__all__":  
            case "__reset__": {
                state.filters = {
                    "read.false": false,
                    "bookmark.true": false,
                    "doc.__null__": false,
                }
                break;
            }

            default: {
                state.filters[payload] = !state.filters[payload];
                break;
            }
        }

        console.log(state.filters);

        return state;
    },
    languageChange(state, payload) {
        state.langConfig = payload;

        return state;
    }
};