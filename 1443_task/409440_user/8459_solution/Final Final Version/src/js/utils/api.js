const headersWithContentType = { "Content-Type": "application/json" };

const checkResponse = (res) => {
	if (res.ok) {
		return res.json();
	}
	return res.json().then((err) => Promise.reject(err));
};


export const getLetters = async (filter={}) => {
	let searchParams = new URLSearchParams(filter).toString();

	const res = await fetch(`/api/letters/?${searchParams}`, {
		method: "GET",
		headers: headersWithContentType,
	});

	return checkResponse(res);
}
