const fs = require("fs");
const path = require("path");


class Model {
    constructor(filename) {
        if (!filename) {
            throw new Error('Filename is required');
        }

        this.filename = filename;

        this.path = path.join(__dirname, filename);
        this.results = null;

        try {
            fs.accessSync(
                this.path, 
                fs.constants.R_OK | fs.constants.W_OK
            );
        } catch (err) {
            throw new Error(`File '${this.path}' not found. Please add the file to the root of the project.`);
        }
    }

    async setInit(value) {
        this.results = value === undefined ? await this.get() : value;

        return this;
    }

    exec() {
        return this.results;
    }

    async get() {
        const jsonRecords = await fs.promises.readFile(this.path, {
            encoding: 'utf8'
        });
        let records = JSON.parse(jsonRecords);

        records.map((record, index) => record.id = index.toString());

        return records;
    }

    async find(params = {}) {
        console.log(params);

        for (let key in params) {
            if (params[key] === '__all__') {
                continue;
            }

            this.results = this.results.filter((record) => {
                if (params[key] === '__null__') {
                    return record.hasOwnProperty(key);
                }
                if (record.hasOwnProperty(key))
                    return record[key].toString() == params[key].toString();
                
                return false;
            });
        }

        return this;
    }

    async countDocuments() {
        this.results = this.results.length;

        return this;
    }

    async limit(limit) {
        this.results = this.results.slice(0, limit);

        return this;
    }

    async skip(skip) {
        this.results = this.results.slice(skip);

        return this;
    }

    async sortByDate() {
        this.results = this.results.sort((a, b) => {
            return  new Date(b['date']) - new Date(a['date']);
        });

        return this;
    }
}

exports.Letter = new Model('db.json');
