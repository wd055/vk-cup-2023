const middelwares = require('./middlewares');
const model = require('./model');


exports.getLettersList = [
    middelwares.searchResults(model.Letter),
    middelwares.sortResults(model.Letter),
    middelwares.paginateResults(model.Letter),
    async (req, res) => {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify(res.results));
        res.end();

        console.log(`${req.method} ${req.url} ${res.statusCode}`);

        return {req, res};
    }
]