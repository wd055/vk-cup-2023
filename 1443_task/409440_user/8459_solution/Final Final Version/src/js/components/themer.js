import Component from './lib/component.js';
import store from '../store/index.js';

import colorConfig from '../config/theme/color.js';
import customConfig from '../config/theme/custom.js';


export default class Themer extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('#settings-theme')
        });
    }

    populateThemeList(element, config) {
        element.innerHTML = '';

        for (const key in config) {
            const themeBoxHTML = `
            <input
                type="radio"
                id="${key}"
                value="${key}"
                name="color-theme"
                hidden
            />
            <label for="${key}" class="theme-box">
                <div
                    class="theme"
                    style="
                        background-color: ${config[key].styles["--color-background"]};
                        background-image: ${config[key].styles["--preview-img"] || "none"};
                    "
                ></div>
                <div class="selected-theme">
                    <span class="ico ico-24">
                        <img src="assets/icons/check-big.svg" alt="Selected" />
                    </span>
                </div>
                <div
                    class="hover-theme"
                >
                    <span
                        class="text"
                        data-i18n="settings.theme.${key}"
                        ${"name" in config[key] ? "" : "hidden"}
                    >
                        Theme name
                    </span>
                </div>
            </label>
            `;

            element.insertAdjacentHTML('beforeend', themeBoxHTML);

            const themeBox = element.querySelector(`#${key}`);
            themeBox && themeBox.addEventListener('change', () => {
                const body = document.querySelector('body');
                body.style = '';

                for (const variable in config[key].styles) {
                    body.style.setProperty(variable, config[key].styles[variable]);
                }

                body.setAttribute('data-theme', config[key]["data-theme"]);
            });
        }
    }

    init() {
        this.populateThemeList(this.element.querySelector('.color-theme-box'), colorConfig);
        this.populateThemeList(this.element.querySelector('.custom-theme-box'), customConfig);

        // set light mode as default

        this.element.querySelector('#light-mode').setAttribute('checked', '');
    }
}