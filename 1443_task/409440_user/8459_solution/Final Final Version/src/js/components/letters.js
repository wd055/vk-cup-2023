import Component from './lib/component.js';
import store from '../store/index.js';

import Actions from './actions.js';
import { formatDateShort, showAttr } from '../utils/utils.js';

export default class Letters extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('#list'),
        });

        this.parent = document.querySelector('#root > main');
    }

    init() {
        store.dispatch("folderChange", {'folder': '__all__'});

        // Add event listener when bottom of list is reached
        this.parent.addEventListener('scroll', (e) => {
            if (store.state.currentLocation !== 'list') {
                return;
            }

            const endPage = e.target.scrollHeight - e.target.scrollTop <= e.target.clientHeight;
            if (endPage) {
                document.querySelector('#loading-list').removeAttribute('hidden');
                store.dispatch('loadMore');
            }
        });
    }

    render() {
        if (store.state.page === 1) {
            this.element.innerHTML = "";  // Clear the list
            this.parent.scrollTop = 0;
        }

        showAttr(this.element, store.state.currentLocation !== 'list', 'hidden');
        if (store.state.currentLocation !== 'list') {
            return;
        }

        document.querySelector('#loading-list').setAttribute('hidden', '');

        store.state.letters.forEach((letter) => {
            const letterHTML = `
            <button
                location="view"
                data-action="locationChange"
                class="letter"
            >
                <div class="unread-dot-box">
                    <div read="${letter.read}" class="read-dot"></div>
                </div>

                <div class="avatar">
                    <input id="checkbox-${letter.id}" type="checkbox" class="checkbox" hidden skip/>
                    <label for="checkbox-${letter.id}" class="checkbox-label" skip>
                        <i class="icon-check" skip></i>
                    </label>
                    <img 
                        src=${letter.author.avatar || "assets/default-avatar.png"}
                        alt="avatar"
                    />
                </div>

                <div class="letter-content-wrapper">
                    <div class="letter-content">
                        <div read="${letter.read}" class="author-name">
                            <span class="text text-overflow text-color-primary">
                                ${letter.author.name} ${letter.author.surname}
                            </span>
                        </div>
                        <div class="icon-flag">
                            <span class="ico ico-20">
                                ${
                                    letter.important ? 
                                    `<i class="icon-exclamation-mark red"></i>` : 
                                    letter.bookmark ? 
                                    `<i class="icon-bookmark-solid red"></i>` :
                                    `<i class="icon-bookmark-regular gray" invisible></i>`
                                }
                            </span>
                        </div>
                        <div class="message-box text-overflow">
                            <p
                                read="${letter.read}"
                                class="message-title text text-overflow text-color-primary"
                            >
                                ${letter.title}
                            </p>
                            <p class="text text-overflow text-color-secondary">
                                ${letter.text}
                            </p>
                        </div>
                        <div class="icons">
                            ${
                                "flag" in letter ? 
                                `<span class="ico ico-24">
                                    <i class="icon-${letter.flag.toLowerCase().replace(/ /g,"-")}"></i>
                                </span>` :
                                ""
                            }
                            ${
                                "doc" in letter ?
                                `<span class="doc-preview-button ico ico-24" tabindex="0" skip>
                                    <i class="icon-attachment text-color-primary" skip></i>
                                </span>` :
                                ""
                            }
                        </div>
                        <p class="text text-color-secondary">
                            ${formatDateShort(letter.date)}
                        </p>
                    </div>
                    <div class="divider"></div>
                </div>
            </button>`;

            this.element.insertAdjacentHTML('beforeend', letterHTML);

            // Add event listeners
            const letterButton = this.element.querySelector('button:last-child');
            letterButton.addEventListener('click', Actions.onClick, false);
            letterButton.addEventListener('click', function (e) {
                store.dispatch('letterChange', {'letter': letter});
            }, false);
        });

        // Hide empty list message if there are letters
        showAttr(
            document.querySelector('#empty-list'),
            store.state.letters.length != 0,
            'hidden'
        );
    }
}