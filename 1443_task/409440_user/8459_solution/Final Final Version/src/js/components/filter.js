import Component from './lib/component.js';
import store from '../store/index.js';

import { showAttr } from '../utils/utils.js';

export default class Filter extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('#filter-button'),
        });
    }

    render() {
        // Copy filters from store
        let filters = {...store.state.filters};

        // if all keys are false, then add __all__ to filters as true
        filters.__all__ = Object.values(filters).every((value) => !value);

        let count = 0, lastKey = null;
        Object.entries(filters).forEach(([key, value]) => {
            const selectIco = this.element.querySelector(`.select-item[filter="${key}"] .ico`);
            const filterIco = this.element.querySelector(`.ico[type="${key}"]`);

            showAttr(selectIco, !value, 'invisible');
            showAttr(filterIco, !value, 'hidden');
            
            count += value;
            lastKey = (value) ? key : lastKey;
        });
        // Filter reset (show/hide)
        const resetFilters = this.element.querySelector('#reset-filters');
        showAttr(resetFilters, filters.__all__, 'hidden');
        
        // Filter text display (plural, singular, or type of filter)
        const text = this.element.querySelector('#filter-text');
        if (count >= 2) {
            text.setAttribute('data-i18n', 'header.filter-button-plural');
        } else {
            if (lastKey == '__all__') {
                text.setAttribute('data-i18n', 'header.filter-button');
                return;
            }
            const filterIco = this.element.querySelector(`.ico[type="${lastKey}"]`);
            text.setAttribute('data-i18n', filterIco.getAttribute('text-i18n'));
        }
    }
}