"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendLetter = exports.moveLetter = exports.getLetterById = exports.getLettersByFolderName = void 0;
var letterService_1 = require("./letterService");
function getLettersByFolderName(req, res) {
    var _a, _b;
    if (!req.url)
        throw new Error("No url");
    var pageNumber = parseInt((_a = req.url.split("page=")[1].split("&")[0]) !== null && _a !== void 0 ? _a : "0");
    var pageSize = parseInt((_b = req.url.split("pageSize=")[1].split("&")[0]) !== null && _b !== void 0 ? _b : "30");
    var filteredData = (0, letterService_1.filterLetters)(req.url.split("api/")[1]);
    res.writeHead(200, { "Content-Type": "application/json" });
    var pageData = filteredData.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize);
    var hasMore = filteredData.length > (pageNumber + 1) * pageSize;
    res.end(JSON.stringify({ pageData: pageData, hasMore: hasMore }));
}
exports.getLettersByFolderName = getLettersByFolderName;
function getLetterById(req, res) {
    if (!req.url)
        throw new Error("No url");
    // /inbox/api/inbox/1?unread=false&bookmarked=false&withAttachments=false
    var letterId = req.url.split("/api/")[1].split("/")[1].split("?")[0];
    var filteredData = (0, letterService_1.filterLetters)(req.url.split("api/")[1]);
    var letter = filteredData.find(function (l) { return l.id === parseInt(letterId); });
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify(letter));
}
exports.getLetterById = getLetterById;
function moveLetter(req, res) {
    if (!req.url)
        throw new Error("No url");
    var letterId = req.url.split("/api/")[1].split("/")[1].split("?")[0];
    var newFolderName = req.url.split("newFolderName=")[1].split("&")[0];
    (0, letterService_1.moveLetterToFolder)(parseInt(letterId), newFolderName);
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify({ message: "Letter moved" }));
}
exports.moveLetter = moveLetter;
function sendLetter(req, res) {
    if (!req.url)
        throw new Error("No url");
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify({ message: "Letter sent" }));
}
exports.sendLetter = sendLetter;
