"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.moveLetterToFolder = exports.filterLetters = exports.folders = void 0;
var data = require("./db.json");
data = data.map(function (letter, index) { return (__assign(__assign({}, letter), { id: index })); });
exports.folders = new Map([
    ["sent", "Отправленные"],
    ["drafts", "Черновики"],
    ["archive", "Архив"],
    ["junk", "Спам"],
    ["trash", "Корзина"],
]);
function filterLetters(url) {
    // inbox/1?unread=false&bookmarked=false&withAttachments=false
    var _a, _b, _c, _d, _e;
    var folderName = url.split("/")[0].split("?")[0];
    var unread = (_a = url.split("unread=")[1].split("&")[0]) !== null && _a !== void 0 ? _a : "false";
    var bookmarked = (_b = url.split("bookmarked=")[1].split("&")[0]) !== null && _b !== void 0 ? _b : "false";
    var withAttachments = (_c = url.split("withAttachments=")[1].split("&")[0]) !== null && _c !== void 0 ? _c : "false";
    var sortOption = ((_d = url.split("sortOption=")[1].split("&")[0]) !== null && _d !== void 0 ? _d : "date");
    var sortDirection = ((_e = url.split("sortDirection=")[1].split("&")[0]) !== null && _e !== void 0 ? _e : "desc");
    var filteredData = [];
    switch (folderName) {
        case "inbox":
            filteredData = data.filter(function (letter) { return letter.folder !== "Отправленные"; });
            break;
        case "important":
            filteredData = data.filter(function (letter) { return letter.important; });
            break;
        default:
            filteredData = data.filter(function (letter) { return letter.folder === exports.folders.get(folderName); });
            break;
    }
    if (unread === "true") {
        filteredData = filteredData.filter(function (letter) { return !letter.read; });
    }
    if (bookmarked === "true") {
        filteredData = filteredData.filter(function (letter) { return letter.bookmark; });
    }
    if (withAttachments === "true") {
        filteredData = filteredData.filter(function (letter) { return letter.doc !== undefined; });
    }
    switch (sortOption) {
        case "date":
            filteredData = filteredData.sort(function (a, b) {
                if (a.date < b.date) {
                    return sortDirection === "asc" ? 1 : -1;
                }
                if (a.date > b.date) {
                    return sortDirection === "asc" ? -1 : 1;
                }
                return 0;
            });
            break;
        case "author":
            filteredData = filteredData.sort(function (a, b) {
                if (a.author.name > b.author.name) {
                    return sortDirection === "asc" ? 1 : -1;
                }
                if (a.author.name < b.author.name) {
                    return sortDirection === "asc" ? -1 : 1;
                }
                return 0;
            });
            break;
        case "title":
            filteredData = filteredData.sort(function (a, b) {
                if (a.title > b.title) {
                    return sortDirection === "asc" ? 1 : -1;
                }
                if (a.title < b.title) {
                    return sortDirection === "asc" ? -1 : 1;
                }
                return 0;
            });
            break;
        default:
            break;
    }
    return filteredData;
}
exports.filterLetters = filterLetters;
function moveLetterToFolder(letterId, newFolderName) {
    data[letterId].folder = exports.folders.get(newFolderName);
}
exports.moveLetterToFolder = moveLetterToFolder;
